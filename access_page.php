<?php
require_once "bootstrap.php";

if(isset($_GET["id"]) && !check_login()){

    if($_GET["id"] == "register_user"){
        $templateParams["titolo"] = "Registrazione Utente";
        $templateParams["nome"] = "user_registration.php";
    }
    else if($_GET["id"] == "register_manager"){
        $templateParams["titolo"] = "Registrazione Attività";
        $templateParams["nome"] = "manager_registration.php";

    } else {
        $templateParams["titolo"] = "Login";
        $templateParams["nome"] = "user_login.php";
    }
}

else if(check_login()){
    if(get_user_type() == "customer"){
        header("location: order_customer.php");
    }
    else if(get_user_type() == "manager"){
        header("location: menu_manager.php");
    }
}
else {
    $templateParams["titolo"] = "Login";
    $templateParams["nome"] = "user_login.php";
}


require "template/base.php";
?>