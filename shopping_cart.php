<?php
require_once "bootstrap.php";

$templateParams["titolo"] = "Carrello";
$templateParams["nome"] = "cart.php";
$templateParams["prezzoTot"] = 0;
if(isset($_COOKIE["cart"])){
    $cookie = $_COOKIE["cart"];
    $cookie = stripslashes($cookie);
    $cart = json_decode($cookie, true);

    foreach(array_keys($cart) as $key){
        $dish_details = $dbc->get_dish_by_id($key);
        $dish_details["idPiatto"] = $key;
        $dish_details["qta"] = $cart[$key];
        $templateParams["prezzoTot"] += ($dish_details["prezzo"] * $dish_details["qta"]);
        if(isset($templateParams["cart"])){
            array_push($templateParams["cart"], $dish_details);
        }
        else{
            $templateParams["cart"] = array($dish_details);
        }
    }
}
else {

}

require "template/base.php";

?>