<?php
require_once "bootstrap.php";

if(get_user_type() == "manager"){
    $templateParams["titolo"] = "Storico ordini";
    $templateParams["nome"] = "order_history_manager.php";
    $barID = $_SESSION["idBar"];
    $templateParams["storico"] = $dbc->get_orders_with_details($barID, "delivered"); 
} 
else {
    header("location: access_page.php?id=login");
}

require "template/base.php";

?>