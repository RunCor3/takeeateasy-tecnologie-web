<?php
    require_once("../bootstrap.php");

    $idOrdine = $_GET["idOrdine"];
    $idStato = $_GET["stato"];
    
    if($dbc->change_order_status($idOrdine, $idStato)){
        $ordine = $dbc->get_order_by_id($idOrdine);
        $idCliente = $ordine["idCliente"];
        $nomeBar = $dbc->get_bar_by_id($_SESSION["idBar"])["nome"];
        $nomeStato = $dbc->get_state_by_id($idStato);
        $user_data["nomeBar"] = $nomeBar;
        $user_data["dataOrdine"] = $ordine["data"];
        $user_data["stato"] = $nomeStato;

        $datiNotificaUser = create_notification("cambio_stato", $user_data);
        $dbc->insert_notification((new DateTime('NOW'))->format('Y-m-d H:i:s'),$datiNotificaUser["titolo"],$datiNotificaUser["contenuto"],$idCliente);

    }
    else{
        echo "errore cambio stato ordine";
    }
?>