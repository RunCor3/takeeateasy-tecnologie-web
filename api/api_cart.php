<?php 
    if(isset($_COOKIE["cart"])){
        $cookie = $_COOKIE["cart"];
        $cookie = stripslashes($cookie);
        $cart = json_decode($cookie, true);
    }

    $idProdotto = $_GET["id"];
    $qta = $_GET["qta"];

    if(isset($cart) && array_key_exists($idProdotto, $cart)){
        $cart[$idProdotto] += $qta;

        if($cart[$idProdotto] <= 0){
            unset($cart[$idProdotto]);
        }
    }
    else{
        $cart[$idProdotto] = $qta;
    }


    setcookie("cart", json_encode($cart), time()+60*60*24, "/");

?>