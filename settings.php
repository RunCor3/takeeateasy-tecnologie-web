<?php
    require_once "bootstrap.php";
    $userType = get_user_type();

    if($userType == "customer"){
        $templateParams["titolo"] = "Profilo - Utente";
        $templateParams["nome"] = "user_settings.php";
    }
    else if($userType == "manager"){
        $templateParams["titolo"] = "Impostazioni - Manager";
        $templateParams["nome"] = "manager_settings.php";
        

        if(isset($_POST["descrizione"])){
            $descrizione = $_POST["descrizione"];
            $dbc->update_bar_description($_SESSION["idBar"], $descrizione);
        }

        if(isset($_FILES["imgBar"]) && $_FILES["imgBar"]["size"] != 0){
            list($result, $img) = uploadImage(UPLOAD_DIR, $_FILES["imgBar"]);

            if($result != 1){
                $_SESSION["errMsg"] = "Errore inserimento immagine";
                header("location: settings.php");
                die();
           }
           else if($result == 1){
            $dbc->update_bar_img($_SESSION["idBar"], $img);    
           }
           
        }

        $templateParams["bar"] = $dbc->get_bar_by_id($_SESSION["idBar"]);
    }
    else if($userType == "visitor"){
        header("location: access_page.php?id=login");
    }
    

    require "template/base.php";
?>