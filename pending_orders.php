<?php
require_once "bootstrap.php";

if(get_user_type() == "manager"){
    $templateParams["titolo"] = "Lista ordini";
    $templateParams["nome"] = "visualize_orders.php";
    $templateParams["stati"] = $dbc->get_possible_states();
    $templateParams["ordini"] = $dbc->get_orders_with_details($_SESSION["idBar"], "pending");

} else {
    header("location: access_page.php?id=login");
}

require "template/base.php";

?>