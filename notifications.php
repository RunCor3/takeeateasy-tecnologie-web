<?php
    require_once "bootstrap.php";

    if(check_login()){
        $templateParams["titolo"] = "Notifiche";
        $templateParams["nome"] = "notifications_page.php";
        $templateParams["notifiche"] = $dbc->get_notifications($_SESSION["email"]);
    }
    else{
        header("location: access_page.php?id=login");
    }

    require "template/base.php";
?>