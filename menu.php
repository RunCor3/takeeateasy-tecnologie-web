<?php
    require_once "bootstrap.php";

    $barID = $_GET["barID"];
    $templateParams["titolo"] = "Menù";
    $templateParams["nome"] = "menu_bar.php";
    $templateParams["barInfo"] =  $dbc->get_bar_by_id($barID);
    $templateParams["menu"] = $dbc->get_bar_menu($barID);
    $qtaFlag = false;
    
    if($templateParams["menu"] != "empty"){
        foreach($templateParams["menu"] as $dish){
            if($dish["qta"] > 0){
                $qtaFlag = true;
                break;
            }
        }

        $cart = deserialize_cart_cookie();
        if(isset($cart)){
            foreach(array_keys($cart) as $idPiatto){
                if($dbc->get_bar_by_dish($idPiatto) != $barID){
                    if(isset($_COOKIE["cart"])){
                        unset($_COOKIE['cart']); 
                        setcookie('cart', null, -1, '/'); 
                    }
                    break;
                }
            }
        }
    }
    // 0   1-   2-   3   4-   5-   6   7-   8-   9   10-
    require "template/base.php";
?>