<?php
    require_once "bootstrap.php";

    $templateParams["titolo"] = "Scegli un Bar";
    $templateParams["nome"] = "bar_list.php";
    $templateParams["bars"] = $dbc->get_bars_and_categories();

    require "template/base.php";
?>