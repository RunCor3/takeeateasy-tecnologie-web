<?php
class DatabaseConnection{
    private $db;

    public function __construct($servername, $username, $password, $dbname, $port){
        $this->db = new mysqli($servername, $username, $password, $dbname, $port);
        if ($this->db->connect_error) {
            die("Connection failed: " . $this->db->connect_error);
        }        
    }

    public function create_activity($nome_attivita, $citta, $indirizzo){
        $query = "INSERT INTO bar(nome, citta, indirizzo) VALUES (?, ?, ?)";
        $stmt = $this->db->prepare($query);

        $stmt->bind_param('sss', $nome_attivita, $citta, $indirizzo);
        if($stmt->execute()){
            return $this->db->insert_id;
        }
        else{
            return -1;
        }
        
    }

    public function register_user($nome, $cognome, $password, $email, $idBar, $citta, $indirizzo){
        $query = "INSERT INTO utente(nome, cognome, password, salt, email, idBar, citta, indirizzo) VALUES (?, ?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);

        $salt = hash('sha256', uniqid(mt_rand(1, mt_getrandmax()), true));
        $salted_password = hash('sha256', $password.$salt);

        $stmt->bind_param('sssssiss', $nome, $cognome, $salted_password, $salt, $email, $idBar, $citta, $indirizzo);

        return $stmt->execute();
    }

    public function login($email, $password){
        $query = "SELECT idUtente, email, nome, cognome, idBar, citta, indirizzo, password, salt FROM utente WHERE email = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $email);

        if($stmt->execute()){
            $result = $stmt->get_result();
            $query_result = $result->fetch_assoc();
            if($query_result == NULL){
                return false;
            }
            else {
                $salted_password = hash('sha256', $password.$query_result["salt"]);
                if($salted_password == $query_result["password"]){
                    unset($query_result["password"]);
                    unset($query_result["salt"]);

                    if($query_result["idBar"] == NULL){
                        unset($query_result["idBar"]);
                    }
                    
                    return $query_result;
                }
                else {
                    return false;
               }
            }
        }
        else{
            return false;
        }
    }

    public function get_bars(){
        $query ="SELECT idBar, nome, indirizzo, img, citta FROM bar";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_bars_by_city($citta){
        $query ="SELECT idBar, nome, indirizzo, img, citta FROM bar WHERE citta LIKE CONCAT('%', ?, '%')";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $citta);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_bars_and_categories($citta = ""){
        $barlist = $this->get_bars_by_city($citta);

        for($i = 0; $i < count($barlist); $i++){
            $barlist[$i]["listaCategorie"] = $this->get_categories($barlist[$i]["idBar"]);
        }
        return $barlist;
    }

    public function get_categories($id){
        $query = "SELECT DISTINCT c.nome FROM categoria c, piatto p WHERE p.idCat = c.idCat AND p.visibile = TRUE AND p.idBar = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $id);
        $stmt->execute();

        $result = $stmt->get_result();
        if (mysqli_num_rows($result) == 0) {
            return "empty";
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC);
        } 
    }

    public function get_bar_menu($idBar){
        $query = "SELECT p.nome, p.ingredienti, p.prezzo, p.img, p.idPiatto, p.qta FROM piatto p WHERE p.idBar = ? AND p.visibile = TRUE";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $idBar);

        $stmt->execute();

        $result = $stmt->get_result();

        if (mysqli_num_rows($result) == 0) {
            return "empty";
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC);
        } 
    }

    public function get_notifications($userEmail){
        $query = "SELECT * from notifiche n, utente u where n.idUtente = u.idUtente AND u.email = ? ORDER BY n.data DESC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $userEmail);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_pending_orders($barId){
        $query = "SELECT u.nome, u.cognome, u.indirizzo AS indirizzoUtente, o.idOrdine AS idOrdine, o.indirizzo, o.data, o.idStato 
        FROM ordine o, utente u, stato s 
        WHERE u.idUtente = o.idCliente 
        AND s.idStato = o.idStato 
        AND o.idStato < 5
        AND o.idBar = ?
        ORDER BY o.data ASC";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $barId);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_delivered_orders($barId){
        $query = "SELECT u.nome, u.cognome, s.nome AS nomeStato, u.indirizzo AS indirizzoUtente, o.idOrdine, o.indirizzo, o.data, o.idStato, o.data 
        FROM ordine o, utente u, stato s 
        WHERE u.idUtente = o.idCliente 
        AND s.idStato = o.idStato 
        AND o.idStato = 5
        AND o.idBar = ?
        ORDER BY o.data ASC";

        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $barId);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_orders_with_details($barId, $orderType){
        if($orderType == "pending"){
            $orderList = $this->get_pending_orders($barId);
        }
        else if($orderType == "delivered"){
            $orderList = $this->get_delivered_orders($barId);
        }
        
        for($i = 0; $i < count($orderList); $i++){
            $orderList[$i]["dettagli"] = $this->get_details($orderList[$i]["idOrdine"]);
        }
        return $orderList;
    }

    public function get_details($orderId){
        $query = "SELECT p.nome, co.qta, co.prezzo FROM contenuto_ordine co, piatto p 
        WHERE p.idPiatto = co.idPiatto 
        AND co.idOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $orderId);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_possible_states(){
        $query = "SELECT * FROM stato";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function get_orders_customer($userEmail) {
        $query = "SELECT b.nome AS nomeBar, s.nome AS nomeStato, o.idOrdine, o.indirizzo, o.data, o.idBar, o.idStato, o.data 
        FROM ordine o, utente u, stato s, bar b 
        WHERE u.idUtente = o.idCliente
        AND s.idStato = o.idStato  
        AND b.idBar = o.idBar
        AND u.email = ? 
        ORDER BY o.data DESC ";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('s', $userEmail);
        $stmt->execute();
        $result = $stmt->get_result();
        $orderList = $result->fetch_all(MYSQLI_ASSOC);

        for($i = 0; $i < count($orderList); $i++){
            $orderList[$i]["dettagli"] = $this->get_details($orderList[$i]["idOrdine"]);
        }
        return $orderList;
    }

    public function get_dish_by_id($idPiatto){
        $query = "SELECT p.nome nome, p.ingredienti ingredienti, p.img img, p.prezzo prezzo, p.qta qta, c.nome nomeCategoria, c.idCat idCat FROM piatto p, categoria c WHERE p.idCat = c.idCat AND p.idPiatto = ?";

        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idPiatto);
        $stmt->execute();
        $result = $stmt->get_result();

        if (mysqli_num_rows($result) == 0) {
            return "empty";
        }
        else{
            return $result->fetch_all(MYSQLI_ASSOC)[0];
        } 
    }

    public function get_all_categories(){
        $query = "SELECT * FROM categoria";
        $stmt = $this->db->prepare($query);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC);
    }

    public function insert_new_dish($nome, $ingredienti, $prezzo, $quantita, $img, $idBar, $idCat){
        echo $nome.$idBar.$idCat;
        $query = "INSERT INTO piatto(nome, ingredienti, prezzo, qta, img, idBar, idCat) VALUES (?, ?, ?, ?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssdisii', $nome, $ingredienti, $prezzo, $quantita, $img, $idBar, $idCat);

        return $stmt->execute();
    }

    public function update_dish($nome, $ingredienti, $prezzo, $quantita, $img, $idBar, $idCat, $idPiatto){
        $query = "UPDATE piatto SET nome = ?, ingredienti = ?, prezzo = ?, qta = ?, img = ?, idBar = ?, idCat = ?
                  WHERE idPiatto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssdisiii', $nome, $ingredienti, $prezzo, $quantita, $img, $idBar, $idCat, $idPiatto);

        return $stmt->execute();
    }

    public function get_bar_by_dish($idPiatto){
        $query = "SELECT b.idBar FROM bar b, piatto p 
                  WHERE p.idBar = b.idBar
                  AND p.idPiatto = ?";

        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idPiatto);
        $stmt->execute();
        $result = $stmt->get_result();

        return $result->fetch_all(MYSQLI_ASSOC)[0]["idBar"];
    }

    public function get_bar_by_id($idBar){
        $query = "SELECT nome, descrizione, img FROM bar WHERE idBar = ?";
        
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('i', $idBar);
        $stmt->execute();
        $result = $stmt->get_result();
        
        return $result->fetch_all(MYSQLI_ASSOC)[0];
    }

    public function insert_new_order($data, $indirizzo, $idStato, $idBar, $idCliente){
        $query = "INSERT INTO ordine(data, indirizzo, idStato, idBar, idCliente) VALUES (?, ?, ?, ?, ?)";
        
        $stmt = $this->db->prepare($query);
        $stmt->bind_param('ssiii', $data, $indirizzo, $idStato, $idBar, $idCliente);
        
        if($stmt->execute()){
            return $this->db->insert_id;
        }
        else{
            return -1;
        }
    }
    
    public function insert_order_detail($idOrdine, $idPiatto, $qta, $prezzo){
        $query = "INSERT INTO contenuto_ordine(idOrdine, idPiatto, qta, prezzo) VALUES (?,?,?,?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("iiid",$idOrdine, $idPiatto, $qta, $prezzo);

        $query_2 = "UPDATE piatto SET qta = qta - ?
                    WHERE idPiatto = ?";
        $stmt_2 = $this->db->prepare($query_2);
        $stmt_2->bind_param("ii", $qta, $idPiatto);
        $stmt_2->execute();

        return $stmt->execute();
    }

    public function get_price_by_id($idPiatto){
        $query = "SELECT prezzo FROM piatto WHERE idPiatto = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("d",$idPiatto);
        $stmt->execute();
        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC)[0]["prezzo"];
    }

    public function delete_order($idOrdine){
        $query_1 = "DELETE FROM contenuto_ordine WHERE idOrdine = ?";
        $query_2 = "DELETE FROM ordine WHERE idOrdine = ?";

        $stmt_1 = $this->db->prepare($query_1);
        $stmt_2 = $this->db->prepare($query_2);

        $stmt_1->bind_param("i",$idOrdine);
        $stmt_2->bind_param("i",$idOrdine);

        $stmt_1->execute();
        $stmt_2->execute();
    }

    public function insert_notification($data, $titolo, $contenuto, $idUtente){
        $query = "INSERT INTO notifiche(data, titolo, contenuto, idUtente) VALUES (?, ?, ?, ?)";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("sssi", $data, $titolo, $contenuto, $idUtente);

        $stmt->execute();
    }

    public function get_manager_by_bar($idBar){
        $query = "SELECT idUtente FROM utente WHERE idBar = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idBar);
        $stmt->execute();

        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC)[0]["idUtente"];
    }

    public function delete_product($idPiatto){
        $query = "UPDATE piatto SET visibile = FALSE WHERE idPiatto = ?";
        
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idPiatto);

        return $stmt->execute();
    }

    public function change_order_status($idOrdine,$idStato){
        $query = "UPDATE  ordine SET idStato = ? WHERE idOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("ii", $idStato, $idOrdine);

        return $stmt->execute();
    }

    public function get_order_by_id($idOrdine){
        $query = "SELECT * FROM ordine WHERE idOrdine = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idOrdine);
        $stmt->execute();

        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC)[0];
    }

    public function get_state_by_id($idStato){
        $query = "SELECT nome FROM stato WHERE idStato = ?";

        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idStato);
        $stmt->execute();

        $result = $stmt->get_result();
        return $result->fetch_all(MYSQLI_ASSOC)[0]["nome"];
    }

    public function update_bar_description($idBar, $descrizione){
        $query = "UPDATE  bar SET descrizione = ? WHERE idBar = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $descrizione, $idBar);

        return $stmt->execute();
    }

    public function update_bar_img($idBar, $img){
        $query = "UPDATE  bar SET img = ? WHERE idBar = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("si", $img, $idBar);

        return $stmt->execute();
    }

    public function delete_notification_by_id($idNotification){
        $query = "DELETE FROM notifiche WHERE idNotifica = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $idNotification);

        return $stmt->execute();
    }

    public function set_notification_as_seen($notId){
        $query = "UPDATE notifiche SET visto = 1 WHERE idNotifica = ?";
        $stmt = $this->db->prepare($query);
        $stmt->bind_param("i", $notId);
        
        return $stmt->execute();
    }
}
?>