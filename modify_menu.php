<?php
     require_once "bootstrap.php";

     if(check_login()){
         $templateParams["titolo"] = "Modifica Menù";
         $templateParams["nome"] = "menu_change.php";
         $templateParams["menu"] = $dbc->get_bar_menu($_SESSION["idBar"]);
     }
     else{
        header("location: access_page.php?id=login");
     }
     
 
     require "template/base.php";
?>