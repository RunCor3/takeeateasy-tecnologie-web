<?php
require_once "bootstrap.php";

if(get_user_type() == "customer"){
    $templateParams["titolo"] = "Storico ordini";
    $templateParams["nome"] = "order_history_customer.php";
    $templateParams["ordini"] = $dbc->get_orders_customer($_SESSION["email"]);
} else {
    header("location: access_page.php?id=login");
}

require "template/base.php";

?>