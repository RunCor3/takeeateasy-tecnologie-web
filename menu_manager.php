<?php

require_once "bootstrap.php";

if(check_login()){
    $templateParams["titolo"] = "Manager Menu";
    $templateParams["nome"] = "manager_menu.html";
}
else{
    header("location: access_page.php?id=login");
}


require "template/base.php";

?>