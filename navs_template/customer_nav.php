<?php ?>

<div class="row nav-container">
  <div class="col-md-12" id="nav-extended">
    <nav class="navbar navbar-expand-lg">
      <ul class="nav col-md-8">
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="order_customer.php">Ordina</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="shopping_cart.php">Carrello</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="history_customer.php">Storico ordini</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="settings.php">Profilo</a>
        </li>
      </ul>
      <ul class="nav col-md-4">
        <li class="col-md-3 offset-md-5 px-0 text-center">
          <button type="button" onclick="location.href='notifications.php'" class="btn bell-btn stretched-link">
              <span class="fa fa-2x fa_custom fa-bell mt-1"></span>
          </button>
        </li>
        <li class="col-md-3 offset-md-1 px-0 text-center">
          <button type="button" onclick="location.href='logout.php'" class="btn logout-btn stretched-link">
              <span class="fa fa-2x fa_custom fa-sign-out mt-1"></span>
          </button>
        </li>
      </ul>
    </nav>
  </div>
  <div class="col-md-12">
    <nav class="navbar navbar-expand-lg" id="nav-collapsed">
      <ul class="nav col-12">
        <li class="col-2 text-center px-0">
          <button type="button" class="btn menu-btn px-0 stretched-link">
            <span class="fa fa-bars fa-2x fa_custom"></span>
          </button>
        </li>
        <li class="col-2 offset-1 text-center px-0">
          <button type="button" onclick="location.href='shopping_cart.php'" class="btn cart-btn px-0 stretched-link">
              <span class="fa fa-shopping-cart fa-2x fa_custom"></span>
          </button>
        </li>
        <li class="col-2 offset-1 text-center px-0">
          <button type="button" onclick="location.href='notifications.php'" class="btn bell-btn px-0 stretched-link">
              <span class="fa fa-2x fa_custom fa-bell"></span>
          </button>
        </li>
        <li class="col-2 offset-1 text-center px-0">
          <button type="button" onclick="location.href='logout.php'" class="btn logout-btn px-0 stretched-link">
              <span class="fa fa-2x fa_custom fa-sign-out"></span>
          </button>
        </li>
      </ul>
    </nav>
  </div>
</div>

<div class="row">
  <div class="sidebar">
    <nav>
      <ul id="sidebar-list">
        <li>
          <a href="order_customer.php" class="nav-link">Ordina</a>
        </li>
        <li>
          <a href="shopping_cart.php" class="nav-link">Carrello</a>
        </li>
        <li>
          <a href="history_customer.php" class="nav-link">Storico ordini</a>
        </li>
        <li>
          <a href="settings.php" class="nav-link">Profilo</a>
        </li>
        <li>
          <a href="logout.php" class="nav-link">Esci dall'account</a>
        </li>
      </ul>
    </nav>
  </div>
</div>