<?php ?>

<div class="row nav-container">
  <div class="col-md-12" id="nav-extended">
    <nav class="navbar navbar-expand-lg">
      <ul class="nav col-md-9">
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="menu_manager.php">Menù gestione</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="pending_orders.php">Visualizza Ordini</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="modify_menu.php">Modifica Menù</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="history_manager.php">Storico ordini</a>
        </li>
        <li class="col-md-2 px-0">
          <a class="nav-link text-center" href="settings.php">Impostazioni</a>
        </li>
      </ul>
      <ul class="nav col-md-3">
        <li class="col-md-4 offset-md-2 px-0 text-center">
          <button type="button" onclick="location.href='notifications.php'" class="btn stretched-link">
              <span class="fa fa-2x fa_custom fa-bell mt-1 bell-btn"></span>
          </button>
        </li>
        <li class="col-md-4 offset-md-1 px-0 text-center">
          <button type="button" onclick="location.href='logout.php'" class="btn stretched-link">
              <span class="fa fa-2x fa_custom fa-sign-out logout-btn mt-1"></span>
          </button>
        </li>
      </ul>
    </nav>
  </div>
  <div class="col-md-12">
    <nav class="navbar navbar-expand-lg" id="nav-collapsed">
      <ul class="nav col-12">
        <li class="col-2 text-center px-0">
          <button type="button" class="btn menu-btn stretched-link">
            <span class="fa fa-bars fa-2x fa_custom menu-btn"></span>
          </button>
        </li>
        <li class="col-2 offset-1 text-center">
          <button type="button" onclick="location.href='notifications.php'" class="btn px-0 stretched-link">
              <span class="fa fa-2x fa_custom fa-bell bell-btn"></span>
          </button>
        </li>
        <li class="col-2 offset-1 text-center px-0">
          <button type="button" onclick="location.href='logout.php'" class="btn px-0 stretched-link">
              <span class="fa fa-2x fa_custom fa-sign-out logout-btn"></span>
          </button>
        </li>
      </ul>
    </nav>
  </div>
</div>

<div class="row">
  <div class="sidebar">
    <nav>
      <ul id="sidebar-list">
        <li>
          <a href="menu_manager.php" class="nav-link">Menù gestione</a>
        </li>
        <li>
          <a href="pending_orders.php" class="nav-link">Visualizza ordini</a>
        </li>
        <li>
          <a href="modify_menu.php" class="nav-link">Modifica menù</a>
        </li>
        <li>
          <a href="history_manager.php" class="nav-link">Storico ordini</a>
        </li>
        <li>
          <a href="settings.php" class="nav-link">Impostazioni</a>
        </li>
        <li>
          <a href="logout.php" class="nav-link">Esci dall'account</a>
        </li>
      </ul>
    </nav>
  </div>
</div>