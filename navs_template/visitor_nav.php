<?php ?>

<div class="row nav-container py-md-1">
  <div class="col-md-12">
    <nav class="navbar navbar-expand-lg" id="nav-extended">
      <ul class="nav col-md-12">
        <li class="col-md-4">
          <a class="nav-link stretched-link text-center" href="access_page.php?id=login">Accedi/Registrati</a>
        </li>
        <li class="col-md-4">
          <a class="nav-link stretched-link text-center" href="order_customer.php">Ordina</a>
        </li>
        <li class="col-md-4">
          <a class="nav-link stretched-link text-center" href="shopping_cart.php">Carrello</a>
        </li>
      </ul>
    </nav>
    <nav class="navbar navbar-expand-lg" id="nav-collapsed">
      <ul class="nav col-12">
        <li class="col-2 text-center px-0">
          <button type="button" class="btn menu-btn stretched-link">
            <span class="fa fa-bars fa-2x fa_custom"></span>
          </button>
        </li>
        <li class="col-2 offset-1 text-center px-0">
          <button type="button" onclick="location.href='shopping_cart.php'" class="btn cart-btn px-0 stretched-link">
            <span class="fa fa-shopping-cart fa-2x fa_custom"></span>
          </button>
        </li>
      </ul>
    </nav>
  </div>
</div>

<div class="row">
  <div class="sidebar">
    <nav>
      <ul id="sidebar-list" class="">
        <li>
          <a href="access_page.php?id=login" class="nav-link">Accedi/Registrati</a>
        </li>
        <li>
          <a href="order_customer.php" class="nav-link">Ordina</a>
        </li>
        <li>
          <a href="shopping_cart.php" class="nav-link">Carrello</a>
        </li>
      </ul>
    </nav>
  </div>
</div>