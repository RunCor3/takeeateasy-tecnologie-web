-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Creato il: Gen 25, 2021 alle 21:32
-- Versione del server: 10.4.14-MariaDB
-- Versione PHP: 7.4.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `takeeateasy`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `bar`
--

CREATE TABLE `bar` (
  `idBar` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `descrizione` varchar(200) DEFAULT NULL,
  `citta` varchar(40) NOT NULL,
  `indirizzo` varchar(40) NOT NULL,
  `img` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `bar`
--

INSERT INTO `bar` (`idBar`, `nome`, `descrizione`, `citta`, `indirizzo`, `img`) VALUES
(2, 'Bar Rossi', 'Bar di mario rossi', 'Cesena', 'Via dell\'università 10', 'index_4.jpg'),
(3, 'Bar Verdi', 'Bar di luca verdi', 'Milano', 'via verdi 15', 'download(1).jpg'),
(4, 'Bar Gialli', 'Bar di Matteo gialli', 'Firenze', 'Via Firenze 987', 'bar.jpg'),
(5, 'Bar Default', NULL, 'Torino', 'Via Torino 12', NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `categoria`
--

CREATE TABLE `categoria` (
  `idCat` int(11) NOT NULL,
  `nome` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `categoria`
--

INSERT INTO `categoria` (`idCat`, `nome`) VALUES
(1, 'Primo'),
(2, 'Pizza'),
(3, 'Panino'),
(4, 'Secondo'),
(5, 'Dessert'),
(6, 'Antipasto'),
(7, 'Insalata'),
(8, 'Contorno'),
(9, 'Orientale'),
(10, 'Caffetteria'),
(11, 'Gelato'),
(12, 'Bibita');

-- --------------------------------------------------------

--
-- Struttura della tabella `contenuto_ordine`
--

CREATE TABLE `contenuto_ordine` (
  `idPiatto` int(11) NOT NULL,
  `idOrdine` int(11) NOT NULL,
  `qta` int(11) NOT NULL CHECK (`qta` > 0),
  `prezzo` decimal(4,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `contenuto_ordine`
--

INSERT INTO `contenuto_ordine` (`idPiatto`, `idOrdine`, `qta`, `prezzo`) VALUES
(2, 1, 2, '8.00'),
(2, 4, 1, '8.00'),
(2, 6, 3, '8.00'),
(3, 3, 3, '6.00'),
(8, 2, 1, '3.99'),
(9, 3, 3, '1.00'),
(10, 1, 2, '6.50'),
(11, 1, 2, '2.00'),
(11, 4, 1, '2.00'),
(11, 5, 1, '2.00'),
(11, 6, 3, '2.00'),
(12, 2, 1, '2.00'),
(14, 5, 1, '6.50');

-- --------------------------------------------------------

--
-- Struttura della tabella `notifiche`
--

CREATE TABLE `notifiche` (
  `idNotifica` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `titolo` varchar(40) NOT NULL,
  `contenuto` varchar(200) NOT NULL,
  `idUtente` int(11) NOT NULL,
  `visto` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `notifiche`
--

INSERT INTO `notifiche` (`idNotifica`, `data`, `titolo`, `contenuto`, `idUtente`, `visto`) VALUES
(1, '2021-01-24 15:23:45', 'Nuovo ordine inviato', 'Tizio hai effettuato correttamente un nuovo ordine presso Bar Verdi.\r\n            Hai pagato 33€.', 1, 1),
(2, '2021-01-24 15:23:45', 'Nuovo ordine ricevuto', 'Bar Verdi hai ricevuto un nuovo ordine da consegnare presso via ciao 123 entro il 2021-01-29', 4, 1),
(3, '2021-01-24 15:25:48', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Verdi è stato aggiornato a PREPARAZIONE.\r\n                        Data e orario previsti di consegna: 29/01/2021 12:05', 1, 0),
(4, '2021-01-24 15:29:52', 'Nuovo ordine inviato', 'Giacomo hai effettuato correttamente un nuovo ordine presso Bar Gialli.\r\n            Hai pagato 5.99€.', 10, 1),
(5, '2021-01-24 15:29:52', 'Nuovo ordine ricevuto', 'Bar Gialli hai ricevuto un nuovo ordine da consegnare presso Via specifica 123 entro il 2021-01-29', 5, 1),
(6, '2021-01-24 15:30:45', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Gialli è stato aggiornato a SPEDITO.\r\n                        Data e orario previsti di consegna: 29/01/2021 03:00', 10, 0),
(7, '2021-01-24 15:31:01', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Verdi è stato aggiornato a CONSEGNATO.\r\n                        Data e orario previsti di consegna: 29/01/2021 12:05', 1, 0),
(8, '2021-01-24 15:32:04', 'Nuovo ordine inviato', 'Tizio hai effettuato correttamente un nuovo ordine presso Bar Rossi.\r\n            Hai pagato 21€.', 1, 0),
(9, '2021-01-24 15:32:04', 'Nuovo ordine ricevuto', 'Bar Rossi hai ricevuto un nuovo ordine da consegnare presso via ciao 123 entro il 2021-01-31', 3, 1),
(10, '2021-01-24 15:32:35', 'Nuovo ordine inviato', 'Tizio hai effettuato correttamente un nuovo ordine presso Bar Verdi.\r\n            Hai pagato 10€.', 1, 0),
(11, '2021-01-24 15:32:36', 'Nuovo ordine ricevuto', 'Bar Verdi hai ricevuto un nuovo ordine da consegnare presso Via specifica 654 entro il 2021-02-03', 4, 0),
(12, '2021-01-24 15:33:13', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Rossi è stato aggiornato a CONFERMATO.\r\n                        Data e orario previsti di consegna: 31/01/2021 12:00', 1, 0),
(13, '2021-01-24 16:20:04', 'Nuovo ordine inviato', 'Tizio hai effettuato correttamente un nuovo ordine presso Bar Verdi.\r\n            Hai pagato 8.5€.', 1, 0),
(14, '2021-01-24 16:20:04', 'Nuovo ordine ricevuto', 'Bar Verdi hai ricevuto un nuovo ordine da consegnare presso Via specifica 321 entro il 2021-01-29', 4, 0),
(15, '2021-01-24 16:20:42', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Verdi è stato aggiornato a SPEDITO.\r\n                        Data e orario previsti di consegna: 03/02/2021 08:30', 1, 0),
(16, '2021-01-24 16:21:26', 'Nuovo ordine inviato', 'Tizio hai effettuato correttamente un nuovo ordine presso Bar Verdi.\r\n            Hai pagato 30€.', 1, 0),
(17, '2021-01-24 16:21:26', 'Nuovo ordine ricevuto', 'Bar Verdi hai ricevuto un nuovo ordine da consegnare presso via ciao 123 entro il 2021-02-06', 4, 0),
(18, '2021-01-24 16:22:20', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Verdi è stato aggiornato a ANNULLATO.\r\n                        Data e orario previsti di consegna: 06/02/2021 09:05', 1, 0),
(19, '2021-01-24 16:22:22', 'Stato dell\'ordine aggiornato', 'Il tuo ordine presso Bar Verdi è stato aggiornato a SPEDITO.\r\n                        Data e orario previsti di consegna: 06/02/2021 09:05', 1, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `ordine`
--

CREATE TABLE `ordine` (
  `idOrdine` int(11) NOT NULL,
  `data` datetime NOT NULL,
  `indirizzo` varchar(40) DEFAULT NULL,
  `idStato` int(11) NOT NULL,
  `idBar` int(11) NOT NULL,
  `idCliente` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `ordine`
--

INSERT INTO `ordine` (`idOrdine`, `data`, `indirizzo`, `idStato`, `idBar`, `idCliente`) VALUES
(1, '2021-01-29 12:05:00', 'via ciao 123', 5, 3, 1),
(2, '2021-01-29 15:00:00', 'Via specifica 123', 4, 4, 10),
(3, '2021-01-31 12:00:00', 'via ciao 123', 2, 2, 1),
(4, '2021-02-03 20:30:00', 'Via specifica 654', 4, 3, 1),
(5, '2021-01-29 12:50:00', 'Via specifica 321', 1, 3, 1),
(6, '2021-02-06 21:05:00', 'via ciao 123', 4, 3, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `piatto`
--

CREATE TABLE `piatto` (
  `idPiatto` int(11) NOT NULL,
  `nome` varchar(30) NOT NULL,
  `ingredienti` varchar(100) NOT NULL,
  `prezzo` decimal(4,2) NOT NULL,
  `qta` int(11) NOT NULL CHECK (`qta` >= 0),
  `img` varchar(50) NOT NULL,
  `idBar` int(11) NOT NULL,
  `idCat` int(11) NOT NULL,
  `visibile` tinyint(1) NOT NULL DEFAULT 1
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `piatto`
--

INSERT INTO `piatto` (`idPiatto`, `nome`, `ingredienti`, `prezzo`, `qta`, `img`, `idBar`, `idCat`, `Visibile`) VALUES
(1, 'Pasta al Pomodoro', 'pasta, pomodoro', '5.90', 0, 'spaghetti-pomodoro-emulsionato-fb_2.jpg', 2, 1, 1),
(2, 'Cheeseburger', 'pane, carne, formaggio', '8.00', 4, 'Cheeseburger.jpg', 3, 3, 1),
(3, 'Pizza Margherita', 'impasto, pomodoro, mozzarella, basilico', '6.00', 18, 'indextest.jpg', 2, 2, 1),
(8, 'Focaccia', 'impasto, rosmarino, olio, fiocchi di sale', '3.99', 24, 'focaccia.jpg', 4, 2, 1),
(9, 'Caffè', 'caffè, zucchero', '1.00', 47, 'caffe.jpg', 2, 10, 1),
(10, 'Carbonara', 'Pasta, guanciale, pecorino, uovo, pepe nero', '6.50', 16, 'carbonara.jpg', 3, 1, 1),
(11, 'Coca cola', 'Coca cola', '2.00', 43, 'coca.jpg', 3, 12, 1),
(12, 'Thè pesca', 'Thè pesca', '2.00', 49, 'the.jpg', 4, 12, 1),
(13, 'American hamburger', 'hamburger, insalata, bacon, maionese, pomodoro', '7.99', 20, 'hamburger_2.jpg', 2, 3, 1),
(14, 'Tagliatelle ragù', 'Pasta, pomodoro, macinato', '6.50', 14, 'tagliatelle-ragu.jpg', 3, 1, 1),
(15, 'Tortellini ragù', 'Pasta, pomodoro, macinato ', '6.50', 15, 'tortellini.jpg', 4, 1, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `stato`
--

CREATE TABLE `stato` (
  `idStato` int(11) NOT NULL,
  `nome` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `stato`
--

INSERT INTO `stato` (`idStato`, `nome`) VALUES
(1, 'inviato'),
(2, 'confermato'),
(3, 'preparazione'),
(4, 'spedito'),
(5, 'consegnato'),
(6, 'annullato');

-- --------------------------------------------------------

--
-- Struttura della tabella `utente`
--

CREATE TABLE `utente` (
  `idUtente` int(11) NOT NULL,
  `nome` varchar(40) NOT NULL,
  `cognome` varchar(40) NOT NULL,
  `password` tinyblob NOT NULL,
  `salt` tinyblob NOT NULL,
  `email` varchar(30) NOT NULL,
  `idBar` int(11) DEFAULT NULL,
  `citta` varchar(40) NOT NULL,
  `indirizzo` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dump dei dati per la tabella `utente`
--

INSERT INTO `utente` (`idUtente`, `nome`, `cognome`, `password`, `salt`, `email`, `idBar`, `citta`, `indirizzo`) VALUES
(1, 'Tizio', 'Caio', 0x66623266393239323066313932336138646230636262643764336537376337353363633264653933613437663666363163363965336634323066343330363065, 0x30366439636263656462636165316232376538303331636131653638323964383435326366313262353666393738346234373464303164366664343536656630, 'tizio.caio@test.it', NULL, 'Roma', 'via ciao 123'),
(3, 'Mario', 'Rossi', 0x36636536323165363532346431333130383137653736353566323132316139323333646665333937646365376462353862313865633661303139333131316236, 0x37353962336432373063336537316164663934623630326461663565353763366232303238643439313163633736373635313530663365353464363832356264, 'mario.rossi@test.it', 2, 'Cesena', 'Via dell\'università 10'),
(4, 'Luca', 'Verdi', 0x34396561623339663034363434336136343432366638623636613230383263306161626131343439336637336263316236363264623763383733323339646336, 0x63323632386163343163386531373536643831643539666566666261383161333833636631356438633266643934353631363163396431333664363032373738, 'luca.verdi@test.it', 3, 'Milano', 'via verdi 15'),
(5, 'Matteo', 'Gialli', 0x33643266303737393938383838393737613435313164663333323337383262303539653032333730343565616235313631353336643733373265363734306263, 0x62633934636661643431643036356233306636346261646130393534356163616133313638376630333236363664376436396333623935333765626665613236, 'matteo.gialli@test.it', 4, 'Firenze', 'Via Firenze 987'),
(6, 'Giorgio', 'Blu', 0x36623965663466306132323362373264663233346131303136373433326539366662343961373136356236356364623862633964653839613765316339623164, 0x37363136366466323934363539383635656339336132323963303430363964656232393166616161393532353835363237336132306661633639643035376431, 'giorgio.blu@test.it', 5, 'Torino', 'Via Torino 12'),
(10, 'Giacomo', 'Gialli', 0x32313639616637356261643930373035613962356234353165643063316161343932623533633336636635383030633836373132633466343532633538653764, 0x64356235646335393631356662653761656632646534336464323036366663386230663535636137396438333363316639326566356639663536353330663736, 'giacomo.gialli@test.it', NULL, 'Firenze', 'Via Firenze 123');

--
-- Indici per le tabelle scaricate
--

--
-- Indici per le tabelle `bar`
--
ALTER TABLE `bar`
  ADD PRIMARY KEY (`idBar`);

--
-- Indici per le tabelle `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`idCat`);

--
-- Indici per le tabelle `contenuto_ordine`
--
ALTER TABLE `contenuto_ordine`
  ADD PRIMARY KEY (`idPiatto`,`idOrdine`),
  ADD KEY `FKcomposizione` (`idOrdine`);

--
-- Indici per le tabelle `notifiche`
--
ALTER TABLE `notifiche`
  ADD PRIMARY KEY (`idNotifica`),
  ADD KEY `FKriceve` (`idUtente`);

--
-- Indici per le tabelle `ordine`
--
ALTER TABLE `ordine`
  ADD PRIMARY KEY (`idOrdine`),
  ADD KEY `FKha` (`idStato`),
  ADD KEY `FKprepara` (`idBar`),
  ADD KEY `FKeffettua` (`idCliente`);

--
-- Indici per le tabelle `piatto`
--
ALTER TABLE `piatto`
  ADD PRIMARY KEY (`idPiatto`),
  ADD KEY `FKproposta` (`idBar`),
  ADD KEY `FKappartenenza` (`idCat`);

--
-- Indici per le tabelle `stato`
--
ALTER TABLE `stato`
  ADD PRIMARY KEY (`idStato`);

--
-- Indici per le tabelle `utente`
--
ALTER TABLE `utente`
  ADD PRIMARY KEY (`idUtente`),
  ADD UNIQUE KEY `IDUTENTE_1` (`email`),
  ADD UNIQUE KEY `FKpossiede_ID` (`idBar`);

--
-- AUTO_INCREMENT per le tabelle scaricate
--

--
-- AUTO_INCREMENT per la tabella `bar`
--
ALTER TABLE `bar`
  MODIFY `idBar` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT per la tabella `categoria`
--
ALTER TABLE `categoria`
  MODIFY `idCat` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT per la tabella `notifiche`
--
ALTER TABLE `notifiche`
  MODIFY `idNotifica` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT per la tabella `ordine`
--
ALTER TABLE `ordine`
  MODIFY `idOrdine` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `piatto`
--
ALTER TABLE `piatto`
  MODIFY `idPiatto` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT per la tabella `stato`
--
ALTER TABLE `stato`
  MODIFY `idStato` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT per la tabella `utente`
--
ALTER TABLE `utente`
  MODIFY `idUtente` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `contenuto_ordine`
--
ALTER TABLE `contenuto_ordine`
  ADD CONSTRAINT `FKcomposizione` FOREIGN KEY (`idOrdine`) REFERENCES `ordine` (`idOrdine`),
  ADD CONSTRAINT `FKcontiene` FOREIGN KEY (`idPiatto`) REFERENCES `piatto` (`idPiatto`);

--
-- Limiti per la tabella `notifiche`
--
ALTER TABLE `notifiche`
  ADD CONSTRAINT `FKriceve` FOREIGN KEY (`idUtente`) REFERENCES `utente` (`idUtente`);

--
-- Limiti per la tabella `ordine`
--
ALTER TABLE `ordine`
  ADD CONSTRAINT `FKeffettua` FOREIGN KEY (`idCliente`) REFERENCES `utente` (`idUtente`),
  ADD CONSTRAINT `FKha` FOREIGN KEY (`idStato`) REFERENCES `stato` (`idStato`),
  ADD CONSTRAINT `FKprepara` FOREIGN KEY (`idBar`) REFERENCES `bar` (`idBar`);

--
-- Limiti per la tabella `piatto`
--
ALTER TABLE `piatto`
  ADD CONSTRAINT `FKappartenenza` FOREIGN KEY (`idCat`) REFERENCES `categoria` (`idCat`),
  ADD CONSTRAINT `FKproposta` FOREIGN KEY (`idBar`) REFERENCES `bar` (`idBar`);

--
-- Limiti per la tabella `utente`
--
ALTER TABLE `utente`
  ADD CONSTRAINT `FKpossiede_FK` FOREIGN KEY (`idBar`) REFERENCES `bar` (`idBar`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
