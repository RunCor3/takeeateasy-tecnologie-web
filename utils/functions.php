<?php
function secure_session_start() {
    $session_name = 'secure_session_id';
    $secure = false;
    $httponly = true;
    ini_set('session.use_only_cookies', 1);
    $cookieParams = session_get_cookie_params();
    session_set_cookie_params($cookieParams["lifetime"], $cookieParams["path"], $cookieParams["domain"], $secure, $httponly); 
    session_name($session_name);
    session_start();
    session_regenerate_id();
}

function select_navbar(){
    if(get_user_type() == "customer"){
        $selected_nav = "customer_nav.php";
    }
    else if(get_user_type() == "manager"){
        $selected_nav = "manager_nav.php";
    }
    else if(get_user_type() == "visitor"){
        $selected_nav = "visitor_nav.php";
    }

    return $selected_nav;
}

function check_login(){
    return isset($_SESSION["user_id"]);
}

function get_user_type(){
    if(check_login()){
        return $_SESSION["user_id"];
    }
    return "visitor";
}

function register_user_data($data){
    if($data["idBar"] == NULL){
        $_SESSION["user_id"] = "customer";
        $_SESSION["customer_id"] = $data["idUtente"];
    }
    else {
        $_SESSION["user_id"] = "manager";
        $_SESSION["idBar"] = $data["idBar"];
    }

    $_SESSION["nome"] = $data["nome"];
    $_SESSION["cognome"] = $data["cognome"];
    $_SESSION["email"] = $data["email"];
    $_SESSION["citta"] = $data["citta"];
    $_SESSION["indirizzo"] = $data["indirizzo"];
}

function uploadImage($path, $image){
    $imageName = basename($image["name"]);
    $imageName = str_replace(" ", "", $imageName);
    $fullPath = $path.$imageName;
    
    $maxKB = 500;
    $acceptedExtensions = array("jpg", "jpeg", "png", "gif");
    //$result = 0;
    $msg = "";
    //Controllo se immagine è veramente un'immagine
    $imageSize = getimagesize($image["tmp_name"]);
    if($imageSize === false) {
        $msg .= "File caricato non è un'immagine! ";
        //$result = 0;
        //return array($result, $msg);
    }
    //Controllo dimensione dell'immagine < 500KB
    if ($image["size"] > $maxKB * 1024) {
        $msg .= "File caricato pesa troppo! Dimensione massima è $maxKB KB. ";
        //$result = 0;
       // return array($result, $msg);
    }

    //Controllo estensione del file
    $imageFileType = strtolower(pathinfo($fullPath,PATHINFO_EXTENSION));
    if(!in_array($imageFileType, $acceptedExtensions)){
        $msg .= "Accettate solo le seguenti estensioni: ".implode(",", $acceptedExtensions);
        //$result = 0;
       // return array($result, $msg);
    }

    //Controllo se esiste file con stesso nome ed eventualmente lo rinomino
    if (file_exists($fullPath)) {
        $i = 1;
        do{
            $i++;
            $imageName = pathinfo(basename($image["name"]), PATHINFO_FILENAME)."_$i.".$imageFileType;
        }
        while(file_exists($path.$imageName));
        $fullPath = $path.$imageName;
    }

    //Se non ci sono errori, sposto il file dalla posizione temporanea alla cartella di destinazione
    if(strlen($msg)==0){
        if(!move_uploaded_file($image["tmp_name"], $fullPath)){
            $msg.= "Errore nel caricamento dell'immagine.";
            //$result = 0;
            //return array($result, $msg);
        }
        else{
            $result = 1;
            $msg = $imageName;
        }
    }
    return array($result, $msg);
}

function deserialize_cart_cookie(){
    if(isset($_COOKIE["cart"])){
        $cookie = $_COOKIE["cart"];
        $cookie = stripslashes($cookie);
        $cart = json_decode($cookie, true);

        return $cart;
    }
}

function create_notification($type, $data){
    switch($type) {
        case "nuovo_ordine":
            $titolo = "Nuovo ordine inviato";
            $contenuto = $_SESSION["nome"]." hai effettuato correttamente un nuovo ordine presso ".$data["nomeBar"].".
            Hai pagato ".$data["prezzoTot"]."€.";
            
            break;
        
        case "ordine_ricevuto":
            $titolo = "Nuovo ordine ricevuto";
            $contenuto = $data["nomeBar"]." hai ricevuto un nuovo ordine da consegnare presso ". $data["indirizzo"]." entro il ". $data["data"];
            break;

        case "cambio_stato":
            $titolo = "Stato dell'ordine aggiornato";
            $contenuto="Il tuo ordine presso ".$data["nomeBar"]." è stato aggiornato a ". strtoupper($data["stato"]).".
                        Data e orario previsti di consegna: ".date_format(date_create($data["dataOrdine"]),"d/m/Y h:i");
            break;

        case "prodotto_esaurito":
            $titolo = "Fine scorte prodotti";
            $contenuto = "Alcuni prodotti sono andati sold-out. 
            Lista prodotti: 
            ".strtoupper($data["piatti"]);
    }
    return array("titolo" => $titolo, "contenuto" => $contenuto);
}


function check_parameters($field){
    return preg_match('/\\d/', $field) > 0;
}

?>