<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4 mt-5 pt-3">
        <h1 class="font-weight-bold">Registra Attività</h1>
    </div>
</div>
<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4 shadow-div py-3 mb-5">
        <form action="access_procedure.php" method="POST" name="manager-registration-form" class="py-3">
            <div class="form-group">
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputName">Nome proprietario:</label>
                    <input name="nome" type="text" class="form-control access" id="inputName" placeholder="Nome proprietario">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputSurname">Cognome proprietario:</label>
                    <input name="cognome" type="text" class="form-control access" id="inputSurname" placeholder="Cognome proprietario">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputActivityName">Nome attività:</label>
                    <input name="nome_attivita" type="text" class="form-control access" id="inputActivityName" placeholder="Nome attività">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputEmail">Email:</label>
                    <input name="email" type="email" class="form-control access" id="inputEmail" placeholder="Email">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputCity">Città:</label>
                    <input name="citta" type="text" class="form-control access" id="inputCity" placeholder="Città">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputActivityAddress">Indirizzo attività:</label>
                    <input name="indirizzo" type="text" class="form-control access" id="inputActivityAddress" placeholder="Indirizzo attività">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3 mb-0">
                    <label class="font-weight-bold" for="inputPassword">Password:</label>
                    <input name="password" type="password" class="form-control access" id="inputPassword" placeholder="Password (minimo 6 caratteri)">
                    <input name="user_type" type="hidden" value="manager">
                </div>
                <div class="row mx-0 mt-4">
                    <button class="btn btn-safe-color mr-0 py-3 col-6 col-sm-6 col-md-6 col-lg-4 offset-3 offset-sm-3 offset-md-4 offset-lg-6 font-weight-bold" id="manager-registration-button">Registrati</button>
                </div>
                <?php 
                    if(isset($_SESSION["access_status"])):
                ?>
                <p class="text-center text-danger font-weight-bold my-4"><?php echo $_SESSION["access_status"] ?></p>
                <?php
                    unset($_SESSION["access_status"]);
                    endif; 
                ?>
            </div>
        </form>
        <div class="row mx-0 mt-4">
            <label for="userLogin" class="col-10 col-md-3 offset-1 offset-md-2 px-0 py-2">Sei già registrato?</label>
            <button class="btn btn-safe-color mr-0 py-3 col-7 col-md-4 offset-1 font-weight-bold" id="userLogin" onclick="location.href='access_page.php?id=login'">Effettua il login</button>
        </div>
        <div class="row mx-0 my-5 pb-5">
            <label for="registerButton" class="col-10 col-md-3 offset-1 offset-md-2 px-0 py-2">Vuoi registrarti come utente?</label>
            <button class="btn btn-safe-color mr-0 py-3 col-7 col-md-4 offset-1 font-weight-bold" id="registerButton" onclick="location.href='access_page.php?id=register_user'">Registrati</button>
        </div>
    </div>
</div>