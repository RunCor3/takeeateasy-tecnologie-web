<div class="row">
    <div class="col-12 col-sm-10 col-md-8 col-lg-6 offset-sm-1 offset-md-1 offset-lg-1 offset-xl-1 mt-5 pt-3">
        <h1 class="font-weight-bold">Modifica Impostazioni</h1>
    </div>
</div>
<div class="row my-5">
    <div class="col-10 col-sm-10 col-md-10 col-lg-4 offset-1 offset-sm-1 offset-md-1 offset-lg-1 mb-5">
        <div class="shadow-div py-3">
            <div class="row mx-0">
                <div class="col-10 col-xl-12 col-lg-4 offset-1 offset-md-2 offset-lg-4 offset-xl-0 ml-xl-4">
                    <h2 class="font-weight-bold">Utente</h2>
                </div>
            </div>
            <div class="row mx-0">
                    <p class="col-6 col-md-4 col-lg-4 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile">Nome:</p>
                    <p class="col-6 col-sm-10 col-md-5 col-lg-5 offset-3 offset-md-0 ml-lg-3 data-profile px-4 px-md-0 pt-1" id="labelName"><?php echo $_SESSION["nome"]?></p>
            </div>
            <div class="row mx-0 my-3">
                    <p class="col-6 col-md-4 col-lg-4 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile">Cognome:</p>
                    <p class="col-6 col-sm-10 col-md-5 col-lg-5 offset-3 offset-md-0 ml-lg-3 data-profile px-4 px-md-0 pt-1" id="labelLastname"><?php echo $_SESSION["cognome"] ?></p>
            </div>
            <div class="row mx-0 my-3">
                    <p class="col-6 col-md-4 col-lg-4 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile">Email:</p>
                    <p class="col-6 col-sm-10 col-md-5 col-lg-5 offset-3 offset-md-0 ml-lg-3 data-profile px-4 px-md-0 pt-1" id="labelEmail"><?php echo $_SESSION["email"] ?></p>
            </div>
            <div class="row mx-0 mt-4">
                <div class="col-10 col-xl-12 col-lg-4 offset-1 offset-md-2 offset-lg-4 offset-xl-0 ml-xl-4">
                    <h2 class="font-weight-bold">Attività</h2>
                </div>
            </div>
            <div class="row mx-0">
                <p class="col-6 col-md-4 col-lg-4 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile">Città:</p>
                <p class="col-6 col-sm-10 col-md-5 col-lg-5 offset-3 offset-md-0 ml-lg-3 data-profile px-4 px-md-0 pt-1" id="labelCity"><?php echo $_SESSION["citta"] ?></p>
            </div>
            <div class="row mx-0 my-3">
                <p class="col-6 col-md-4 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile">Indirizzo:</p>
                <p class="col-6 col-sm-10 col-md-5 col-lg-5 offset-3 offset-md-0 ml-lg-3 data-profile px-4 px-md-0 pt-1" id="labelAddress"><?php echo $_SESSION["indirizzo"] ?></p>
            </div>
        </div>
    </div>
    <div class="col-10 col-lg-4 offset-1 offset-sm-1 offset-md-1 offset-lg-2">
        <form class="shadow-div form-group py-3 mt-3 mt-lg-0" action="settings.php" enctype="multipart/form-data" method="POST">
            <div class="row mx-0">
                <div class="col-10 col-xl-10 col-lg-4 offset-1 offset-md-1 offset-lg-4 offset-xl-1">
                    <h3 class="font-weight-bold">Modifica dettagli attività</h3>
                </div>
            </div>
            <div class="row mx-0">
                <p class="col-6 col-md-4 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile">Nome :</p>
                <p class="col-6 col-sm-10 col-md-5 offset-3 offset-md-0 ml-lg-3 data-profile px-4 px-md-0 pt-1" id="labelBarName"><?php echo $templateParams["bar"]["nome"] ?></p>
            </div>
            <div class="row mx-0 my-3">
                <label class="col-6 col-md-8 col-lg-8 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile" for="inputDescription">Descrizione Attività:</label>
                <textarea rows="3" name="descrizione" class="form-control access col-sm-10 offset-1 textarea-sm" id="inputDescription" placeholder="Nessuna descrizione"><?php echo $templateParams["bar"]["descrizione"] ?></textarea>
            </div>
            <div class="row mx-0 my-5">
                <label class="col-6 col-md-8 col-lg-8 offset-md-2 offset-3 offset-lg-1 px-4 px-md-0 info-profile" for="inputBarImg">Immagine Attività:</label>
                <input type="file" name="imgBar" class="form-control-file access col-sm-10 offset-lg-1" id="inputBarImg">
                <?php
                    if(isset($_SESSION["errMsg"])){
                        echo "<p class='offset-1 font-weight-bold'>".$_SESSION["errMsg"]."</p>";
                        unset($_SESSION["errMsg"]);
                    }
                ?>
            </div>
            <div class="row mx-0 mt-5 mb-3">
                <input type="submit" class="btn btn-safe-color col-8 col-sm-5 col-lg-8 col-xl-6 offset-1 offset-md-2 offset-lg-2 offset-xl-1 font-weight-bold py-3" value="Applica Modifiche">
            </div>
        </form>
    </div>
</div>