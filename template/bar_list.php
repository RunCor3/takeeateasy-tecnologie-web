<div class="row">
    <div class="col-9 col-md-6 col-lg-4 offset-2 offset-md-3 offset-lg-3 col-xl-4 offset-xl-2 pt-3 mt-4 mb-5">
        <h1 class="font-weight-bold">Seleziona il Bar</h1>
    </div>
</div>
<?php if(isset($_GET["msg"])) :?>
    <div class="row">
        <div class="col-8 col-md-4 col-lg-4 offset-2 offset-md-3 offset-lg-3 col-xl-4 offset-xl-2 pt-3 mb-3">
            <p class="error-text"><?php echo $_GET["msg"] ?></p>
        </div>
    </div>
<?php endif; ?>
<div class="row">
    <div class="col-8 col-sm-7 col-md-5 col-lg-5 col-xl-3 offset-2 offset-md-3 offset-lg-3 offset-xl-2 px-0">
        <form action="#" method="GET" name="login-form" class="py-3">
                <div class="row pl-4 mb-1">
                    <label class="font-weight-bold" for="inputCity">Filtra per Città:</label>
                </div>
                <div class="row">
                    <div class="col-8 pl-4">
                        <input name="citta" type="text" id="inputCity" class="form-control" placeholder="Città">
                    </div>
                    <div class="col-4">
                        <input type="submit" class="btn btn-safe-color mr-0 col-12 font-weight-bold" id="filterCity" value="Filtra">
                    </div>
                </div>
        </form>
    </div>  
</div>
<div class="row col-10 offset-1 cards-container" id="bar-list">
    <?php $i = 0; ?>
    <?php foreach($templateParams["bars"] as $bar): ?>
        <?php if($bar["listaCategorie"] != "empty"){
            $categories = implode(", ",array_column($bar["listaCategorie"],"nome"));
        }
        ?>
        <div class="card shadow-div col-md-6 col-8 col-sm-6 col-lg-3 offset-2 offset-sm-3 offset-lg-2 col-xl-3 offset-xl-1 my-5" onclick="location.href='menu.php?barID=<?php echo $bar["idBar"] ?>'">
            <img class="card-img-top bar-card-img mt-3 ml-lg-0" 
                 src="<?php if($bar["img"] != ""){ 
                                echo UPLOAD_DIR.$bar["img"];} 
                            else { 
                                echo "upload/test-bar-img.jpg";}
                        ?>" alt="<?php echo $bar['nome'] ?>">
            <div class="card-body">
                <h5 class="card-title"><?php echo $bar["nome"]?></h5>
                <p class="card-text"><?php echo $categories ?></p>
                <p class="card-text"><?php echo $bar["citta"].", ".$bar["indirizzo"]?></p>
            </div>
        </div>
    <?php $i++; ?>
    <?php endforeach; ?>
</div>
