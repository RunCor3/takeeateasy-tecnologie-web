<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-6 offset-1 offset-sm-1 offset-md-1 offset-lg-1 offset-xl-2 mt-5 pt-3">
        <h1 class="font-weight-bold">Visualizza Profilo</h1>
    </div>
</div>
<div class="row">
    <div class="col-10 col-lg-10 col-xl-8 offset-1 offset-lg-1 offset-xl-2 py-3 mb-5">
        <div class="shadow-div form-group py-3">
            <div class="row col-lg-12 col-xl-12">
                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-6 offset-lg-1 offset-xl-0">
                        <p class="font-weight-bold col-10 col-md-3 col-lg-5 col-xl-5 offset-1 offset-sm-2 offset-md-1 offset-lg-0 d-inline-block label pl-0 pl-xl-4 info-profile">Nome e cognome:</p>
                        <p class="d-inline access col-10 col-sm-8 col-md-6 col-lg-6 col-xl-6  offset-1 offset-sm-2 offset-md-0 offset-xl-0 data-profile" id="labelNome"><?php echo $_SESSION["nome"]." ".$_SESSION["cognome"]?></p>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-6 offset-lg-1 offset-xl-0">
                    <p class="font-weight-bold col-10 col-md-3 col-lg-5 col-xl-5 offset-1 offset-sm-2 offset-md-1 offset-lg-0 d-inline-block label pl-0 pl-xl-5 info-profile">Email:</p>
                    <p class="d-inline access col-10 col-sm-8 col-md-6 col-lg-6 col-xl-6 offset-1 offset-sm-2 offset-md-0 offset-xl-0 data-profile" id="labelEmail"><?php echo $_SESSION["email"]?></p>
                </div>
            </div>
            <div class="row col-lg-12 col-xl-12">
                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-6 offset-lg-1 offset-xl-0">
                    <p class="font-weight-bold col-10 col-md-3 col-lg-5 col-xl-5 offset-1 offset-sm-2 offset-md-1 offset-lg-0 d-inline-block label pl-0 pl-xl-4 info-profile">Città:</p>
                    <p class="d-inline access col-10 col-sm-8 col-md-6 col-lg-6 col-xl-6  offset-1 offset-sm-2 offset-md-0 offset-xl-0 data-profile" id="labelCity"><?php echo $_SESSION["citta"]?></p>
                </div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-10 col-xl-6 offset-lg-1 offset-xl-0">
                    <p class="font-weight-bold col-10 col-md-3 col-lg-5 col-xl-5 offset-1 offset-sm-2 offset-md-1 offset-lg-0 d-inline-block label pl-0 pl-xl-5 info-profile">Indirizzo:</p>
                    <p class="d-inline access col-10 col-sm-8 col-md-6 col-lg-6 col-xl-6 offset-1 offset-sm-2 offset-md-0 offset-xl-0 data-profile" id="labelAddress"><?php echo $_SESSION["indirizzo"]?></p>
                </div>
            </div>
        </div>
    </div>
</div>