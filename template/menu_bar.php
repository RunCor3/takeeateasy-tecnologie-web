<div class="row mt-4">
    <div class="col-12 col-sm-10 col-md-10 col-lg-8 offset-0 offset-sm-1 offset-md-1 offset-lg-2 text-center">
        <img class="bar-img" src="<?php if($templateParams["barInfo"]["img"] != ""){ 
                echo UPLOAD_DIR.$templateParams["barInfo"]["img"];} 
            else { 
                echo "upload/test-bar-img.jpg";
            }?>" alt="Immagine del Bar <?php echo $templateParams["barInfo"]["nome"]?>">
    </div>
</div>
<div class="row pt-3">
    <div class="col-10 col-md-10 col-lg-8 offset-1 offset-md-1 offset-lg-2 text-center">
        <h1 class="font-weight-bold"><?php echo $templateParams["barInfo"]["nome"]?></h1>
        <p><?php echo $templateParams["barInfo"]["descrizione"]?></p>
    </div>
</div>
<div class="row">
    <h2 class="col-3 font-weight-bold col-md-2 col-lg-2 offset-2 offset-md-1 offset-lg-2">Menù</h2>
</div>
<div class="row">
    <div class="col-8 col-md-10 col-lg-8 offset-2 offset-md-1 offset-lg-2 py-3">
        <section>
            <?php if(!$qtaFlag):?>
                <h3 class="text-center">Non ci sono prodotti nel menù</h3>
            <?php else: ?>
                <?php $i = 1 ?>
                <?php foreach($templateParams["menu"] as $dish): ?>
                    <?php if($dish["qta"] > 0):?>
                    <article class="item-div row px-0 py-3 mb-4">
                        <div class="item-img col-6 col-md-3 col-lg-2 offset-3 offset-md-0 offset-lg-0 text-center">
                            <img src="<?php echo UPLOAD_DIR.$dish["img"] ?>" alt="immagine del piatto <?php echo $dish['nome'] ?>">
                        </div>
                        <div class="item-content col-12 col-md-6 col-lg-6 mt-3 mt-md-0">
                            <p class="col-12 col-md-12 col-lg-12 item-text font-weight-bold" id="nome_<?php echo $i ?>"><?php echo $dish["nome"] ?></p>
                            <p class="col-12 col-md-12 col-lg-12 item-text" id="ingredienti_<?php echo $i ?>">Ingredienti: <?php echo $dish["ingredienti"] ?></p>
                            <p class="mt-4 col-12 col-md-12 col-lg-12 item-text" id="prezzo_<?php echo $i ?>">Prezzo: <?php echo $dish["prezzo"]."€" ?></p>
                        </div>
                        <div class="item-buttons col-12 col-md-3 col-lg-4">
                            <div class="row">
                                <button id="btn-minus_<?php echo $i ?>" class="btn btn-safe-color btn-sm btn-block col-2 col-md-2 col-lg-2 offset-3 offset-md-2 offset-lg-5 minus">
                                    <span class="fa fa-minus fa-custom"></span>
                                </button>
                                <p class="col-2 col-md-3 col-lg-2 text-center qta-label font-weight-bold" id="qta_<?php echo $i ?>">1</p>
                                <button id="btn-plus_<?php echo $i ?>" class="btn btn-safe-color btn-sm btn-block col-2 col-md-2 col-lg-2 plus">
                                    <span class="fa fa-plus fa-custom"></span>
                                </button>
                            </div>
                            <form>
                                <input id="idPiatto_<?php echo $i ?>" type="hidden" value="<?php echo $dish["idPiatto"] ?>">
                            </form> 
                            <div class="row mt-5">
                                <button class="btn btn-safe-color btn-block col-4 col-md-7 col-lg-6 offset-4 offset-md-2 offset-lg-5 mt-3 btn-aggiungi font-weight-bold" id="aggiungi_<?php echo $i ?>">Aggiungi</button>
                            </div>
                        </div>
                    </article>
                    <?php $i=$i+1 ?>
                    <?php endif; ?>
                <?php endforeach; ?>
            <?php endif; ?>
        </section>
        <div class="row my-5">
                <button onclick="location.href='shopping_cart.php'" class="btn btn-safe-color btn-block col-6 col-md-4 offset-3 offset-md-4 offset-lg-4 text-center mt-3 py-3 stretched-link font-weight-bold">Vai al Carrello</button>
        </div>
    </div>
</div>