        <div class="row">
            <div class="col-12 col-md-10 offset-md-1 px-4">
                <header class="mt-4">
                    <h1 class="font-weight-bold">Riepilogo Ordine</h1>
                </header>
                <section class="mt-4">
                    <?php if(!isset($templateParams["cart"]) ):?>
                    <h2 class="text-center">Non ci sono prodotti nel carrello</h2>
                    <?php else: ?>
                        <?php foreach($templateParams["cart"] as $element):?>
                    <article class="py-3 shadow-div cart-card mb-3" id="articolo_<?php echo $element["idPiatto"] ?>">
                        <div class="row mx-0">
                            <header class="col-12 col-md-6 my-3 text-center">
                                <div class="row mx-0">
                                    <div class="col-4 col-md-4"><img src="<?php echo UPLOAD_DIR.$element["img"] ?>" class="rounded" alt="Immagine del piatto <?php echo $element["nome"]?>"/></div>
                                    <h2 class="col-8 col-md-8 px-0 h5 nome-piatto"><?php echo $element["nome"] ?></h2>
                                </div>
                            </header>
                            <section class="col-12 col-md-6 text-center mt-3">
                                <div class="row mx-0">
                                    <div class="col-6 col-md-8">
                                        <form action="#" method="GET">
                                            <input type="hidden" name="dishId" value="<?php echo $element["idPiatto"]?>"/>
                                            <input type="button" class="d-inline-block btn btn-safe-color mr-2 minus-btn" value="-"/>
                                            <p id="quantitaAttuale_<?php echo $element["idPiatto"]?>" class=" qta-label d-inline-block mr-2"><?php echo $element["qta"] ?></p>
                                            <input type="button" class="d-inline-block btn btn-safe-color plus-btn" value="+"/>
                                        </form>
                                    </div>
                                    <p class="d-inline-block col-6 col-md-4 p-prezzo" id="prezzo_<?php echo $element["idPiatto"] ?>"><strong><?php echo $element["prezzo"] ?> €</strong></p>
                                </div>
                            </section>
                        </div>
                    </article>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </section>
                
                <div class="row">
                    <div class="mt-4 mb-5 col-12 col-lg-8 offset-lg-2">
                        <footer class="py-4 shadow-div">
                            <form action="confirmation.php" method="POST">
                                <div class="form-group row text-center mt-3 mx-0">
                                    <label for="giornoRitiro" class="col-6 cart-info">Giorno di ritiro:</label>
                                    <input type="date" id="giornoRitiro" class="offset-2 col-3 form-control" name="giornoRitiro"/>
                                </div>
                                <div class="form-group row text-center mt-3 mx-0">
                                    <label for="orarioRitiro" class="col-6 cart-info">Orario di ritiro:</label>
                                    <input type="time" id="orarioRitiro" class="offset-2 col-3 form-control" name="orarioRitiro"/>
                                </div>
                                <div class="form-group row text-center mx-0">
                                    <p class="cart-info col-6">Importo totale:</p>
                                    <p id="importoTotale" class="col-6 p-prezzo"><strong><?php echo $templateParams["prezzoTot"] ?>€</strong></p>
                                </div>
                                <div class="form-group row text-center mx-0 mt-4">
                                    <p class="col-12 cart-info"><strong>Seleziona indirizzo di consegna:</strong></p>
                                </div>
                                <fieldset>
                                    <div class="form-group row text-center mx-0">
                                        <div class="col-12 col-md-6 text-center">
                                            <input type="radio" id="indirizzoUtente" value="indirizzo_utente" name="tipoIndirizzo" checked="checked"/>
                                            <label for="indirizzoUtente" class="clickableLabel cart-info">Indirizzo utente</label>
                                        </div>
                                        <div class="col-12 col-md-6 text-center">
                                            <input type="radio" id="indirizzoSpecifico" value="indirizzo_specifico" name="tipoIndirizzo"/>
                                            <label for="indirizzoSpecifico" class="clickableLabel cart-info">Indirizzo specifico</label>
                                        </div>
                                    </div>
                                </fieldset>
                                <div id="addressForm" class="form-group row text-center mx-0 mb-4">
                                    <label for="indirizzo" class="col-12 cart-info">Indirizzo:</label>
                                    <input type="text" id="indirizzo" class="col-8 offset-2 form-control" name="indirizzo"/>
                                </div>
                                <div class="form-group text-center mb-4">
                                    <input type="submit" class="btn cart-info btn-safe-color font-weight-bold px-5 py-3" value="Ordina"/>
                                </div>
                                <?php if(isset($_GET["msg"])):?>
                                <div class="row text-center mx-0">
                                    <p class="text-center text-danger col-12"><strong><?php echo $_GET["msg"]?></strong></p>
                                </div>
                                <?php endif; ?>
                            </form>
                        </footer>
                    </div>
                </div>  
            </div>
        </div>