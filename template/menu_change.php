<div class="row">
    <div class="col-10 col-md-6 col-lg-4 offset-2 offset-md-1 offset-lg-2 pt-3 mt-4">
        <h1 class="font-weight-bold">Modifica Menù</h1>
    </div>
</div>
<div class="row">
    <div class="col-6 col-md-4 col-lg-2 offset-2 offset-md-1 offset-lg-2 mt-4 mb-2">
        <button class="btn btn-safe-color btn-block font-weight-bold" onclick="location.href='modify_item.php'" id="add">Aggiungi piatto</button>
    </div>
</div>
<?php if(isset($_GET["msg"])): ?>
<div class="row mx-0 mt-4 mb-2">
    <p class="col-12 offset-2 offset-md-1 offset-lg-2 px-0 error-text"><strong><?php echo $_GET["msg"]; unset($_GET["msg"]) ?></strong></p>
</div>
<?php endif; ?>
<div class="row mx-0">
    <div class="col-8 col-md-10 col-lg-8 offset-2 offset-md-1 offset-lg-2 py-3">
        <section>
            <?php if($templateParams["menu"] == "empty"): ?>
                <h2 class="text-center font-weight-bold">Non ci sono prodotti da modificare</h2>
            <?php else: ?>
                <?php foreach($templateParams["menu"] as $dish): ?>
                    <article class="item-div row px-0 py-3 mb-4" id="article_<?php echo $dish["idPiatto"] ?>">
                        <div class="item-img col-6 col-md-2 col-lg-2 offset-3 offset-md-0 offset-lg-0 text-center">
                            <img src="<?php echo UPLOAD_DIR.$dish["img"] ?>" alt="Immagine piatto <?php echo $dish["nome"] ?>">
                        </div>
                        <div class="item-content col-12 col-md-7 col-lg-6">
                            <p class="col-12 col-md-12 col-lg-12 item-text"><?php echo $dish["nome"]; ?></p>
                            <p class="col-12 col-md-12 col-lg-12 item-text product-ingredients"><strong>Ingredienti: </strong><?php echo $dish["ingredienti"]; ?></p>
                            <p class="col-12 col-md-12 col-lg-12 item-text">Prezzo: <?php echo $dish["prezzo"]; ?>€</p>
                        </div>
                        <div class="item-buttons col-12 col-md-3 col-lg-4">
                            <button class="btn btn-danger btn-block col-6 col-md-12 col-lg-7 offset-3 offset-md-0 offset-lg-5 btn-rimuovi font-weight-bold" id="rimuovi_<?php echo $dish["idPiatto"] ?>">Rimuovi</button>
                            <button class="btn btn-safe-color btn-block col-8 col-md-12 col-lg-7 offset-2 offset-md-0 offset-lg-5 modify font-weight-bold" onclick="location.href='modify_item.php?item_id=<?php echo $dish['idPiatto']?>'">Modifica piatto</button>
                        </div>
                    </article>
                <?php endforeach; ?>
            <?php endif; ?>
        </section>
    </div>
</div>







