<div class="row">
    <div class="col-10 col-md-10 col-lg-8 offset-1 offset-md-1 offset-lg-2 pt-3 mt-4">
        <header>
            <h1 class="font-weight-bold">Notifiche</h1>
        </header>
        <section class="mt-4 mb-5">
            <?php if (count($templateParams["notifiche"]) == 0):?>
            <h2 class="font-weight-bold h3 text-center">Non ci sono notifiche</h2>
            <?php else:?>
                <?php foreach($templateParams["notifiche"] as $notifica):?>
                <article id="article_<?php echo $notifica["idNotifica"]?>" class="py-3 mt-1 shadow-div order-card mb-3">
                    <section>
                        <div class="row mx-0">
                            <div class="col-12 col-lg-12">
                                <div class="row col-12 mx-0 mx-sm-0 mx-md-0 mx-lg-2 mx-xl-0">
                                    <p class="col-8 notification-info"><?php echo date_format(date_create($notifica["data"]),"d/m/Y H:i")?></p>
                                    <div class="col-4 col-xl-4 pr-xl-4">
                                        <button type="button" id="elimina_<?php echo $notifica["idNotifica"]?>" class="btn btn-sm btn-danger col-8 col-sm-4 col-md-4 col-lg-4 col-xl-2 offset-8 offset-sm-10">
                                            <span class="fa fa-times fa_custom"></span>
                                        </button>
                                    </div>
                                </div>
                                <h3 id="not-<?php echo $notifica["idNotifica"]?>" class="ml-4 <?php if(!$notifica["visto"]){ echo "font-weight-bold";}?>"><?php echo $notifica["titolo"] ?><?php if($notifica["visto"]){ echo "<span class='ml-4 fa fa-notification-check fa-custom fa-check'></span>";}?></h3>
                            </div>
                            <div class="col-3 col-md-3 offset-md-9 pt-2 details-div text-center">
                                <form class="col-12 offset-3" method="GET" action="#">
                                    <input type="button" class="btn btn-safe-color btn-details notification-btn" value="Dettagli"/>
                                    <input type="hidden" name="notId" value="<?php echo $notifica["idNotifica"]?>"/>
                                    <input type="hidden" name="seen" value="<?php echo $notifica["visto"]?>"/>
                                </form>
                            </div>
                        </div>
                    </section>
                    <footer class="px-2 bg-white pt-4">
                        <p class="col-10 ml-3 notification-info"><?php echo $notifica["contenuto"]?></p>
                    </footer>
                </article>
                <?php endforeach;?>
            <?php endif;?>
        </section>
    </div>
</div>
