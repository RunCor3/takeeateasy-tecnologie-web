
<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4 mt-5 pt-3">
        <h1 class="font-weight-bold">Registrati</h1>
    </div>
</div>
<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4  py-3 mb-5 shadow-div">
        <form name="user-registration-form" class="py-3" action="access_procedure.php" method="POST">
            <div class="form-group">
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputName">Nome:</label>
                    <input name="nome" type="text" class="form-control access" id="inputName" placeholder="Nome">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputSurname">Cognome:</label>
                    <input name="cognome" type="text" class="form-control access" id="inputSurname" placeholder="Cognome">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputEmail">Email:</label>
                    <input name="email" type="email" class="form-control access" id="inputEmail" placeholder="Email">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputCity">Città:</label>
                    <input name="citta" type="text" class="form-control access" id="inputCity" placeholder="Città">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3">
                    <label class="font-weight-bold" for="inputAddress">Indirizzo:</label>
                    <input name="indirizzo" type="text" class="form-control access" id="inputAddress" placeholder="Indirizzo">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 px-0 py-3 mb-0">
                    <label class="font-weight-bold" for="inputPassword">Password:</label>
                    <input name="password" type="password" class="form-control access" id="inputPassword" placeholder="Password (minimo 6 caratteri)">
                    <input name="user_type" type="hidden" value="customer">
                </div>
                <div class="row mx-0 mt-4">
                    <button class="btn btn-safe-color mr-0 py-3 col-6 col-sm-6 col-md-6 col-lg-4 offset-3 offset-sm-3 offset-md-4 offset-lg-6 font-weight-bold" id="user-registration-button">Registrati</button>
                </div>
                <?php 
                    if(isset($_SESSION["access_status"])):
                ?>
                <p class="text-center text-danger font-weight-bold my-4"><?php echo $_SESSION["access_status"] ?></p>
                <?php
                    unset($_SESSION["access_status"]);
                    endif; 
                ?>
            </div>
        </form>
        <div class="row mx-0 mt-4">
                    <label for="loginButton" class="col-10 col-md-3 offset-1 offset-md-2 px-0 py-2">Sei già registrato?</label>
                    <button onclick="location.href='access_page.php?id=login'" id="loginButton" class="btn btn-safe-color mr-0 py-3 col-7 col-md-4 offset-1 stretched-link font-weight-bold">Effettua il login</button>
                </div>
                <div class="row mx-0 my-5 pb-5">
                    <label for="managerButton" class="col-10 col-md-3 offset-1 offset-md-2 px-0 py-2">Vuoi lavorare con noi?</label>
                    <button onclick="location.href='access_page.php?id=register_manager'" id="managerButton" class="btn btn-safe-color mr-0 py-3 col-7 col-md-4 offset-1 stretched-link font-weight-bold">Registra attività</button>
        </div>
    </div>
</div>