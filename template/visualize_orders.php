<div class="row">
    <div class="col-12 col-md-10 offset-md-1 px-4">
        <header class="mt-4">
            <h1 class="font-weight-bold">Ordini ricevuti:</h1>
        </header>
        <section class="mt-4">
            <?php if (count($templateParams["ordini"]) == 0):?>
            <h2 class="text-center">Non ci sono ordini</h2>
            <?php else:?>
                <?php foreach($templateParams["ordini"] as $ordine):?>
                <article class="py-3 px-2 shadow-div order-card mb-3">
                    <section class="mb-3">
                        <div class="row mx-0">
                            <div class="col-2 d-none d-md-block text-center"><span class="fa fa-4x fa-user"></span></div>
                            <div class="col-6 col-sm-8 col-md-7">
                                <h2 class="h4"><?php echo $ordine["nome"] . " " . $ordine["cognome"]?></h2>
                                <p class="order-info"><strong>Indirizzo: </strong><?php echo $ordine["indirizzo"];?></p>
                                <p class="order-info"><?php echo date_format(date_create($ordine["data"]), "d/m/Y H:i")?></p>
                            </div>
                            <div class="col-6 col-sm-4 col-md-3 col-lg-2 text-center">
                                <form method="GET" action="#">
                                    <label class="order-info" for="stato_<?php echo $ordine["idOrdine"]?>"><strong>Stato</strong></label>
                                    <select name="stato" class="btn-block mt-2 order-info" id="stato_<?php echo $ordine["idOrdine"]?>">
                                        <?php foreach($templateParams["stati"] as $stato):?>
                                        <option value="<?php echo $stato["idStato"]?>" <?php if($ordine["idStato"] == $stato["idStato"]){echo "selected";}?>><?php echo $stato["nome"]?></option>
                                        <?php endforeach;?>
                                    </select>
                                    <input type="button" class="btn btn-sm btn-safe-color btn-details mt-4 btn-block order-info" value=" Dettagli "/>
                                    <input type="hidden" name="orderId" value="<?php echo $ordine["idOrdine"]?>"/>
                                </form>
                            </div>
                        </div>
                    </section>
                    <footer class="px-2 bg-white">
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th class="order-info-sm" id="nomeProdotto_<?php echo $ordine["idOrdine"]?>">Prodotto</th>
                                    <th class="order-info-sm" id="quantita_<?php echo $ordine["idOrdine"]?>">Q.tà</th>
                                    <th class="order-info-sm" id="importo_<?php echo $ordine["idOrdine"]?>">Importo</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($ordine["dettagli"] as $dettaglio) :?>
                                <tr>
                                    <td class="order-info-sm" headers="nomeProdotto_<?php echo $ordine["idOrdine"]?>"><?php echo $dettaglio["nome"]?></td>
                                    <td class="order-info-sm" headers="quantita_<?php echo $ordine["idOrdine"]?>">
                                        <p class="d-inline-block mr-2"><?php echo $dettaglio["qta"]?></p>
                                    </td>
                                    <td class="order-info-sm" headers="importo_<?php echo $ordine["idOrdine"]?>">€<?php echo $dettaglio["prezzo"]?></td>
                                </tr>
                                <?php endforeach;?>
                            </tbody>
                        </table>
                    </footer>
                </article>
                <?php endforeach;?>
            <?php endif;?>
        </section>
    </div>
</div>