<div class="row">
    <div class="col-12 col-md-10 offset-md-1 px-4">
        <header class="mt-4">
            <h1 class="font-weight-bold">Storico ordini effettuati</h1>
        </header>
        <section class="mt-4 mb-5">
            <?php if (count($templateParams["storico"]) == 0): ?>
            <h2 class="text-center">Non ci sono ordini nello storico</h2>
            <?php else:?>
            <?php foreach($templateParams["storico"] as $ordine):?>
            <article class="py-3 px-2 shadow-div order-card mb-3">
                <section>
                    <div class="row mx-0">
                        <div class="col-2 d-none d-md-block text-center"><span class="fa fa-4x fa-user"></span></div>
                        <div class="col-7 col-sm-8 col-md-7">
                            <h2><?php echo $ordine["nome"] . " " . $ordine["cognome"]?></h2>
                            <p class="order-info"><strong>
                                <?php echo $ordine["indirizzo"];?>
                            </strong></p>
                            <p class="order-info"><?php echo date_format(date_create($ordine["data"]),"d/m/Y H:i")?></p>
                        </div>
                        <div class="col-5 col-sm-4 col-md-3 text-center">
                            <p class="order-info"><?php echo $ordine["nomeStato"] ?></p>
                            <form method="GET" action="#">
                                <input type="button" class="btn btn-sm btn-safe-color btn-details order-info" value="Dettagli"/>
                            </form>
                        </div>
                    </div>
                </section>
                <footer class="px-2 bg-white">
                    <table class="table text-center">
                        <thead>
                            <tr>
                                <th class="order-info-sm" id="nomeProdotto">Prodotto</th>
                                <th class="order-info-sm" id="quantita">Q.tà</th>
                                <th class="order-info-sm" id="importo">Importo</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach($ordine["dettagli"] as $dettaglio) : ?>
                            <tr>
                                <td class="order-info-sm" headers="nomeProdotto"><?php echo $dettaglio["nome"] ?></td>
                                <td class="order-info-sm" headers="quantita">
                                    <p id="quantitaAttuale1" class="d-inline-block mr-2"><?php echo $dettaglio["qta"] ?></p>
                                </td>
                                <td class="order-info-sm" headers="importo"><?php echo $dettaglio["prezzo"] ?>€</td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </footer>
            </article>
            <?php endforeach; ?>
            <?php endif;?>
        </section>
    </div>
</div>