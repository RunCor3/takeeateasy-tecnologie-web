<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4 mt-5 pt-3">
        <h1 class="font-weight-bold"><?php echo (isset($templateParams["action"]) && $templateParams["action"] == "modify")? "Modifica" : "Aggiungi" ?> Piatto</h1>
    </div>
</div>
<div class="row">
    <div class="col-10 col-sm-8 col-md-6 col-lg-4 offset-1 offset-sm-2 offset-md-3 offset-lg-4  py-3 mb-5">
        <form action="modify_item.php" method="POST" enctype="multipart/form-data" class="shadow-div py-3">
            <div class="form-group">
                <div class="col-10 col-md-8 offset-1 offset-md-2 pt-3">
                    <label class="font-weight-bold" for="inputProductName">Nome piatto:</label>
                    <input type="text" name="nomePiatto" class="form-control access" id="inputProductName" placeholder="Nome" required value="<?php if (isset($templateParams["piatto"])){echo $templateParams["piatto"]["nome"];}?>"/>
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 pt-3">
                    <label class="font-weight-bold" for="ingredients">Ingredienti:</label>
                    <textarea name="ingredienti" class="form-control access" id="ingredients" placeholder="Ingredienti" required><?php if (isset($templateParams["piatto"])){echo $templateParams["piatto"]["ingredienti"];}?></textarea>
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 pt-3">
                    <label class="font-weight-bold" for="inputPrice">Prezzo:</label>
                    <input type="number" step="0.01" name="prezzo" class="form-control access" id="inputPrice" required placeholder="Prezzo" value="<?php if (isset($templateParams["piatto"])){echo $templateParams["piatto"]["prezzo"];}?>">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 pt-3">
                    <label class="font-weight-bold" for="inputProductType">Tipologia:</label>
                    <select class="form-control" id="inputProductType" name="categoria" required>
                        <option value="">Inserisci Tipologia piatto</option>
                        <?php foreach($templateParams["categorie"] as $categoria):?>
                        <option value="<?php echo $categoria["idCat"]?>" <?php if (isset($templateParams["piatto"]) && $templateParams["piatto"]["idCat"] == $categoria["idCat"]){echo "selected";}?>><?php echo $categoria["nome"]?></option>
                        <?php endforeach;?>
                    </select>
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 pt-3">
                    <label for="inputProductAmount" class="font-weight-bold">Quantità:</label>
                    <input type="number" name="qta" class="form-control access" id="inputProductAmount" placeholder="Quantità" required value="<?php if (isset($templateParams["piatto"])){echo $templateParams["piatto"]["qta"];}?>">
                </div>
                <div class="col-10 col-md-8 offset-1 offset-md-2 pt-3">
                    <label for="inputProductImg" class="font-weight-bold">Immagine:</label>
                    <input type="file" name="imgPiatto" class="form-control-file access" id="inputProductImg" <?php if($templateParams["action"] == "add"){ echo "required"; }?>/>
                    <input type="hidden" name="action" value="<?php echo $templateParams["action"] ?>">
                    <input type="hidden" name="idPiatto" value="<?php echo $_GET["item_id"] ?>">
                </div>
                <div class="row mx-0 pb-0">
                    <button class="btn btn-safe-color mr-0 py-3 col-6 mb-0 col-md-6 col-lg-5 offset-1 offset-md-2 offset-lg-2 font-weight-bold">Applica Modifiche</button>
                </div>
                <div class="row mx-0">
                    <button class="btn btn-danger mr-0 py-3 col-6 col-md-6 col-lg-5 offset-1 offset-md-2 offset-lg-2 font-weight-bold" id="returnToMenu">Torna al Menù</button>
                </div>
            </div>
        </form>
    </div>
</div>