<!DOCTYPE html>
<html lang="it">

<head>
  <meta charset="UTF-8" />
  <meta name="description" content="Homepage" />
  <meta name="keywords" content="HTML" />
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css"/>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"/>
  <!-- local reference to font awesome-->
  <!--<link rel="stylesheet" href="font-awesome-icons/css/font-awesome.min.css"/>-->

  <!-- jQuery library -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>

  <!-- Popper JS -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

  <!-- Latest compiled JavaScript -->
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  <!-- CryptoJS Library -->
  <script src="https://cdn.jsdelivr.net/npm/crypto-js@3.1.9-1/crypto-js.js"></script>

  <!-- JQuery -->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

  <script src="js/app.js"></script>

  <script src="js/bar_loading.js"></script>

  <script src="js/cart_loading.js"></script>

  <script src="js/cart_update.js"></script>

  <script src="js/product_update.js"></script>

  <script src="js/order_state_update.js"></script>

  <script src="js/notification_update.js"></script>

  <link rel="stylesheet" type="text/css" href="css/style.css"/>

  <title><?php echo $templateParams["titolo"]?></title>
</head>

<body class="bg-light">
  <div class="container-fluid" id="container">
    <div class="row header-container">
      <div class="col-12">
        <header class="py-10">
          <img class="col-8 offset-2 rounded mx-auto d-block logo" src="img/TakeEatEasy_logo.png" alt="TakeEatEasy Touch it, Take it" onclick="location.href='index.php'">
        </header>
      </div>
    </div>
    <?php 
      if(isset($templateParams["nav"])){
        require("navs_template/" . $templateParams["nav"]);
      }
    ?>
    <main>
      <?php 
      if(isset($templateParams["nome"])){
          require($templateParams["nome"]);
      }
      ?>
    </main>

    <!-- Footer -->
    <div class="row footer-div">
      <div class="col-12 col-md-12 px-0">
        <footer class="pt-3 pb-3" id="page-footer">
          <ul class="nav">
            <li class="col-md-2 col-12 text-left">
              <a class="" href="index.php">Homepage</a>
            </li>
            <li class="col-md-2 col-12 text-left">
              <a class="" href="access_page.php?id=login">Accedi o Registrati</a>
            </li>
            <li class="col-md-2 col-12 text-left">
              <a class="" href="about_us_page.php">Chi siamo?</a>
            </li>
            <li class="col-md-2 col-12 text-left">
              <a class="" href="contacts_page.php">Contatti</a>
            </li>
          </ul>
        </footer>
      </div>
    </div>
  </div>
</body>

</html>