<div class="row">
    <div class="col-12 col-md-10 offset-md-1 px-4">
        <header class="mt-4">
            <h1 class="font-weight-bold">Ordini effettuati</h1>
        </header>
        <section class="mt-4 mb-5">
            <?php if (count($templateParams["ordini"]) == 0): ?>
            <h2 class="text-center">Non ci sono ordini nello storico</h2>
            <?php else:?>
            <?php foreach($templateParams["ordini"] as $ordine):?>
                <article class="py-3 px-2 shadow-div order-card mb-3">
                    <section>
                        <div class="row mx-0">
                            <div class="col-md-2 d-none d-md-block"><img src="<?php echo UPLOAD_DIR.$dbc->get_bar_by_id($ordine["idBar"])["img"]?>" class="rounded history-img" alt="Immagine del Bar <?php echo $ordine["nomeBar"]?>"/></div>
                            <div class="col-8">
                                <h2><?php echo $ordine["nomeBar"]?></h2>
                                <p class="order-info"><strong><?php echo $ordine["nomeStato"]?></strong></p>
                                <p class="order-info"><?php echo date_format(date_create($ordine["data"]), "d/m/Y H:i") ?></p>
                            </div>
                            <div class="col-4 col-md-2 text-center">
                                <form method="GET" action="#">
                                    <input type="button" class="btn btn-sm btn-safe-color btn-details order-info font-weight-bold" value="Dettagli"/>
                                </form>
                            </div>
                        </div>
                    </section>
                    <footer class="px-2 bg-white">
                        <p class="order-info"><strong>Indirizzo di consegna:</strong> <?php echo $ordine["indirizzo"]?></p>
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th class="order-info-sm" id="nomeProdotto">Prodotto</th>
                                    <th class="order-info-sm" id="quantita">Q.tà</th>
                                    <th class="order-info-sm" id="importo">Importo</th>
                                </tr>
                            </thead>
                            <tbody>
                            <?php foreach($ordine["dettagli"] as $dettaglio) : ?>
                                <tr>
                                    <td class="order-info-sm" headers="nomeProdotto"><?php echo $dettaglio["nome"] ?></td>
                                    <td class="order-info-sm" headers="quantita">
                                        <p id="quantitaAttuale1" class="d-inline-block mr-2"><?php echo $dettaglio["qta"] ?></p>
                                    </td>
                                    <td class="order-info-sm" headers="importo"><?php echo $dettaglio["prezzo"] ?>€</td>
                                </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </footer>
                </article>
                <?php endforeach; ?>
            <?php endif;?>
        </section>
    </div>
</div>