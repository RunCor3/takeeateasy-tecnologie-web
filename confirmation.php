<?php
require_once "bootstrap.php";

if(get_user_type() != "customer"){
    $_SESSION["access_status"] = "Devi prima effettuare l'accesso per inviare un ordine";
    header("location: access_page.php?id=login");
    die();
}

/*Vengono recuperati i prodotti nel carrello e le informazioni relative all'ordine*/

$cart = deserialize_cart_cookie();
$data = $_POST["giornoRitiro"];
$ora = $_POST["orarioRitiro"];
$dateTime = date('Y-m-d H:i:s', strtotime("$data $ora"));
$tipoIndirizzo = $_POST["tipoIndirizzo"];
if($tipoIndirizzo == "indirizzo_utente"){
    $indirizzo = $_SESSION["indirizzo"];
}
else if($tipoIndirizzo == "indirizzo_specifico"){
    $indirizzo = $_POST["indirizzo"];
}
$idStato = 1;
$idBar = $dbc->get_bar_by_dish(array_key_first($cart));
$idCliente = $_SESSION["customer_id"];

$availability_flag = true;


/*Vien controllata la disponibilità dei prodotti richiesti, nel caso uno o più di essi non sia disponibile viene mostrata
la quantità massima acquistabile*/

foreach(array_keys($cart) as $idPiatto){
    $qta = $cart[$idPiatto];
    $final_qta = check_product_availability($dbc, $qta, $idPiatto);
    if($qta != $final_qta){
        $availability_flag = false;
        $cart[$idPiatto] = $final_qta;
        if ($final_qta == 0){
            unset($cart[$idPiatto]);
        }
    }
}

/*Uno o più prodotti non sono disponibili perciò si aggiorna il carrello e viene ricaricata la pagina*/
if(!$availability_flag){
    $msg = "Uno o più prodotti nel carrello erano in quantità eccessiva rispetto alla disponibilità";
    setcookie("cart", json_encode($cart), time() + 60 * 60 * 24, "/");
    header("location: shopping_cart.php?msg=".$msg);
    die();
}

/*Inserimento dell'ordine*/
$idOrdine = $dbc->insert_new_order($dateTime, $indirizzo, $idStato, $idBar, $idCliente);

if($idOrdine == -1){
    $msg = "L'ordine non è andato a buon fine";
    header("location: order_customer.php?msg=".$msg);
    die();
}

$prezzoTot = 0;

/*Dopo aver inserito l'ordine, questa parte aggiunge al db il contenuto dell'ordine*/
$soldoutDishesName = array();
foreach(array_keys($cart) as $idPiatto){
    $qta = $cart[$idPiatto];    
    $prezzo = $dbc->get_price_by_id($idPiatto);
    $prezzoTot += $prezzo * $qta;
    if(!$dbc->insert_order_detail($idOrdine, $idPiatto, $qta, $prezzo)){
        $dbc->delete_order($idOrdine);
        $msg = "L'ordine non è andato a buon fine";
        header("location: order_customer.php?msg=".$msg);
        die();
    }
    //Viene costruita la lista dei prodotti non disponibili
    if($dbc->get_dish_by_id($idPiatto)["qta"] <= 0){
        $nomePiatto = $dbc->get_dish_by_id($idPiatto)["nome"];
        array_push($soldoutDishesName, $nomePiatto);
    } 
}

//Viene controllata la presenza di prodotti non disponibili e in caso non sia vuota, viene inviata una notifica al manager dell'attività
if (count($soldoutDishesName) > 0){
    $soldoutDishes = implode(",", $soldoutDishesName);
    $idManager = $dbc->get_manager_by_bar($idBar);
    $user_data["piatti"] = $soldoutDishes;
    $datiNotifica = create_notification("prodotto_esaurito", $user_data);
    $dbc->insert_notification((new DateTime('NOW'))->format('Y-m-d H:i:s'), $datiNotifica["titolo"], $datiNotifica["contenuto"], $idManager);
}

//L'ordine è stato effettuato con successo, si procede ad inviare una notifica di conferma al cliente ed una al manager
$idManager = $dbc->get_manager_by_bar($idBar);
$user_data["prezzoTot"] = $prezzoTot;
$user_data["nomeBar"] = $dbc->get_bar_by_id($idBar)["nome"];
$manager_data["nomeBar"] = $user_data["nomeBar"];
$manager_data["data"] = $data;
$manager_data["indirizzo"] = $indirizzo;
$datiNotificaUser = create_notification("nuovo_ordine", $user_data);
$datiNotificaManager = create_notification("ordine_ricevuto", $manager_data);
$dbc->insert_notification((new DateTime('NOW'))->format('Y-m-d H:i:s'),$datiNotificaUser["titolo"],$datiNotificaUser["contenuto"],$idCliente);
$dbc->insert_notification((new DateTime('NOW'))->format('Y-m-d H:i:s'),$datiNotificaManager["titolo"],$datiNotificaManager["contenuto"],$idManager);


setcookie("cart", "", time() - 10, "/");

$templateParams["titolo"] = "Conferma ordine";
$templateParams["nome"] = "order_confirmed.html";

require "template/base.php";

//funzione che controlla se un prodotto nel carrello è in quantità eccessiva rispetto alla disponibilità
function check_product_availability($dbc, $qta, $idPiatto){
    $available = $dbc->get_dish_by_id($idPiatto)["qta"];

    if ($available < $qta){
        return $available;
    }
    else if ($qta <= $available){
        return $qta;
    }
}

?>