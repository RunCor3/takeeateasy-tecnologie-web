function printBars(barList){
    let result = "";
    const UPLOAD_DIR = "./upload/";

    for(let i=0; i < barList.length; i++){
        let categories;
        barList[i]["img"] = barList[i]["img"] == null ?  UPLOAD_DIR + "test-bar-img.jpg" : UPLOAD_DIR + barList[i]["img"];
        if(barList[i]["listaCategorie"] != "empty"){
            categories = barList[i]["listaCategorie"][0]["nome"];
        
            for(let j=1; j < barList[i]["listaCategorie"].length; j++){
                categories += ", " + barList[i]["listaCategorie"][j]["nome"];
            }
        }
        else{
            categories = "Nessuna categoria presente";
        }

        if(barList[i]["img"] == null){
            barList[i]["img"] = "test-bar-img.jpg";
        }
        let bar = `<div class="card shadow-div col-md-6 col-8 offset-2 offset-md-3 col-xl-3 offset-xl-1 my-5">
                    <img class="card-img-top mt-3" src="${barList[i]["img"]}" alt="Card image cap">
                    <div class="card-body">
                        <h5 class="card-title">${barList[i]["nome"]}</h5>
                        <p class="card-text">${categories}</p>
                        <p class="card-text">${barList[i]["citta"] + ", " + barList[i]["indirizzo"]}</p>
                        <a href="menu.php?barID=${barList[i]["idBar"]}" class="stretched-link"></a>
                    </div>
                </div>`;
        result += bar;
    }

    return result;
}


$(document).ready(function(){
      $("input#inputCity").on("change paste keyup", function(){
        const inputCityValue = $("input#inputCity").val();

        $.ajax({
            dataType: "json",
            url: "api/api_bar.php",
            data: {city: inputCityValue},
            success: function(data){
                
                let bars = printBars(data);
                const main = $("div#bar-list");
                main.html(bars);
            }
          });
    });
});
