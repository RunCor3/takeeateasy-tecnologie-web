$(document).ready(function () {

    $("article.order-card > footer").hide();
    $("div#addressForm").hide();

    const sidebar = $(".sidebar");
    $("html").click(function (e) {
        //console.log(e.target);
        //console.log(sidebar[0]);

        if ($(".sidebar").hasClass("active")) {
            $(".sidebar").hide();
            $(".sidebar").removeClass("active");
        }
    });

    $(".menu-btn").click(function (e) {
        e.preventDefault();
        e.stopPropagation();

        if ($(".sidebar").hasClass("active")) {
            $(".sidebar").hide();
            $(".sidebar").removeClass("active");
        } else {
            $(".sidebar").show();
            $(".sidebar").addClass("active");
        }
    });
    
    $("input#indirizzoUtente").click(function(e){
        $("div#addressForm").slideUp();
    });

    $("input#indirizzoSpecifico").click(function(e){
        $("div#addressForm").slideDown();
    });

    $(window).on('resize', function (event) {
        let windowWidth = $(window).width();
        if (windowWidth > 768) {
            $(".sidebar").hide();
            $(".sidebar").removeClass("active");
        }
    });

    $("input.btn-details").click(function (e) {
        e.preventDefault();
        $(this).parents("section").next().slideToggle();
    });

    $("button#user-registration-button").click(function (e) {
        e.preventDefault();
        $(".errorParag").remove();
        let msg = checkUserRegistration(this.form);
        if(msg !== ""){
            let parag = `<p class="text-center col-12 text-danger font-weight-bold mt-4 errorParag">${msg}</p>`;
            $(this).parent().append(parag);
        } else {
            this.form = passwordHashing(this.form, this.form.password);
            this.form.submit();
        }
    });

    $("button#manager-registration-button").click(function (e) {
        e.preventDefault();
        $(".errorParag").remove();
        let msg = checkBarRegistration(this.form);
        msg += checkUserRegistration(this.form);
        if(msg !== ""){
            let parag = `<p class="text-center col-12 text-danger font-weight-bold mt-4 errorParag">${msg}</p>`;
            $(this).parent().append(parag);
        } else {
            this.form = passwordHashing(this.form, this.form.password);
            this.form.submit();
        }
    });

    $("button#login-button").click(function (e) {
        e.preventDefault;
        this.form = passwordHashing(this.form, this.form.password);
        this.form.submit();
    });


    $("button.minus").click(function (e) {
        const btnID = $(this).attr('id');
        let [qta, qtaSelector] = parseElementID(btnID);
        if(qta > 1){
            qta -= 1;
            $(qtaSelector).text(qta);
        }
    });

    $("button#returnToMenu").click(function(e){
        e.preventDefault();
        window.location.href = "modify_menu.php";
    });

    $("button.plus").click(function (e) {
        const btnID = $(this).attr('id');
        let [qta, qtaSelector] = parseElementID(btnID);

        qta = parseInt(qta) + 1 ;
        $(qtaSelector).text(qta);
    });

    function parseElementID(btnID){
        
        const itemID = btnID.substr((btnID.length - 1));

        const qtaPreSelector = "p#qta_";
        const qtaSelector = qtaPreSelector.concat(itemID);
        const qta = $(qtaSelector).text();

        return [qta, qtaSelector];
    }

    function passwordHashing(form, password) {
        const p = document.createElement("input");

        form.appendChild(p);
        p.name = "password_hashed";
        p.type = "hidden"
        p.value = CryptoJS.SHA256(password.value);
        password.value = "";
        return form;
     }

    function isValueIncorrect(formValue)
    {
        const regex = /\d/g;
        let result = regex.test(formValue);
        return formValue.length == 0 || result;
    }    

    function checkUserRegistration(form)
    {
        let msg = "";
        if(form.nome == null || isValueIncorrect(form.nome.value)){
            correct = false;
            msg += "Inserire nome (senza numeri)</br>"
        }
        if(form.cognome == null || isValueIncorrect(form.cognome.value)){
            correct = false;
            msg += "Inserire cognome (senza numeri)</br>"
        }
        if(form.citta == null || isValueIncorrect(form.citta.value)){
            correct = false;
            msg += "Inserire città (senza numeri)</br>"
        }
        if(form.password.value.length < 6){
            correct = false;
            msg += "Password troppo corta (minimo 6 caratteri)";
        }
        return msg;
    }

    function checkBarRegistration(form)
    {
        let msg = "";
        if(form.nome_attivita == null || form.nome_attivita.value.length <= 0){
            correct = false;
            msg += "Inserire nome attività</br>"
        }
        return msg;
    }
    
});
