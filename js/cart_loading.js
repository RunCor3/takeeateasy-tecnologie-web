$(document).ready(function(){
    $("button.btn-aggiungi").click(function (e) {
        e.preventDefault();
        const btnID = $(this).attr('id');
        const itemID = btnID.substr((btnID.length - 1));

        const idPiattoSelector = "input#idPiatto_";
        const qtaSelector = "p#qta_";

        const idProdotto = $(idPiattoSelector.concat(itemID)).val();
        const qta = $(qtaSelector.concat(itemID)).text();
        
        $.ajax({
            url: "api/api_cart.php",
            data: {id: idProdotto, qta: qta},
            success: function(data){
                const check = `<span class="mx-3 offset-5 fa fa-cartcheck fa-custom fa-check"></span>`;
                $("#" + btnID).html(check);

                setTimeout(function(){
                    $("#" + btnID).text("Aggiungi");
                },2000);
            }
          });
    });
});