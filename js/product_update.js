$(document).ready(function(){
    $("button.btn-rimuovi").click(function(e){
        let idPiatto = $(this).attr("id");
        idPiatto = idPiatto.replace("rimuovi_", "");


        $.ajax({
            url: "api/api_product.php",
            data: {id: idPiatto},
            success: function(data){
                $("article#article_" + idPiatto).remove();
            }
        });
    });
});

