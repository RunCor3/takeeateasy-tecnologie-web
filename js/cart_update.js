$(document).ready(function(){
    $(".minus-btn").click(function(e){
        e.preventDefault();
        const idPiatto = $(this).parent().children("input[type='hidden']").val();
        const qtaSelector = $("p#quantitaAttuale_" + idPiatto);
        const prezzoTotSelector = $("#importoTotale > strong");
        let prezzo = parseFloat($("p#prezzo_" + idPiatto + " > strong").text()).toFixed(2);
        let current_qta = qtaSelector.text();
        let prezzoTot = parseFloat(prezzoTotSelector.text()).toFixed(2);

        if(current_qta > 0){
            $.ajax({
                url: "api/api_cart.php",
                data: {id: idPiatto, qta: -1},
                success: function(){
                    current_qta -= 1;
                    prezzoTot = prezzoTot - prezzo;
                    prezzoTot = Math.round((prezzoTot + Number.EPSILON) * 100) / 100;
                    console.log(prezzoTot);
                    
                    prezzoTotSelector.text(prezzoTot + " €");
                    qtaSelector.text(current_qta);

                    if(current_qta <= 0){
                        $("article#articolo_" + idPiatto).remove();
                    }
                }
              });
        }
    });

    $(".plus-btn").click(function(e){
        e.preventDefault();
        const idPiatto = $(this).parent().children("input[type='hidden']").val();
        const qtaSelector = $("p#quantitaAttuale_" + idPiatto);
        const prezzoTotSelector = $("#importoTotale > strong");
        let prezzo = parseFloat($("p#prezzo_" + idPiatto + " > strong").text()).toFixed(2);
        let current_qta = qtaSelector.text();
        let prezzoTot = parseFloat(prezzoTotSelector.text()).toFixed(2);

        $.ajax({
            url: "api/api_cart.php",
            data: {id: idPiatto, qta: 1},
            success: function(){
                current_qta = parseInt(current_qta) + 1;
                prezzoTot = parseFloat(prezzoTot) + parseFloat(prezzo);
                prezzoTot = Math.round((prezzoTot + Number.EPSILON) * 100) / 100;
                console.log(prezzoTot);
                prezzoTotSelector.text(prezzoTot + " €");
                qtaSelector.text(current_qta);
            }
        });
        
    });


});