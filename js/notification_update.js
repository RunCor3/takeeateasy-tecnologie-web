$(document).ready(function(){
    $(document).on("click", "button[id*='elimina']", function(e){
        const idNotification = ($(this).attr("id")).replace("elimina_", "");

        $.ajax({
            url: "api/api_notifications.php",
            data: {idNotification: idNotification},
            success: function(data){
                $("article#article_" + idNotification).remove();
            }
          });
    });


    $("input.notification-btn").click(function(e){
        e.preventDefault();
        const hiddenSeen = $(this).nextAll().eq(1);
        if(hiddenSeen.val() == 0){
            const notificationId = $(this).next().val()
            $.ajax({
                url: "api/api_notifications.php",
                data: {notId: notificationId},
                success: function(data){ 
                    $("h3#not-"+ notificationId).removeClass("font-weight-bold");
                    const tickSpan = `<span class='ml-4 fa fa-notification-check fa-custom fa-check'></span>`;
                    $("h3#not-" + notificationId).append(tickSpan);
                    hiddenSeen.val(1);
                }
            });
        }
    });
});