<?php
require_once "bootstrap.php";

if(isset($_POST["user_type"])){
    if($_POST["user_type"] == "login"){
        login_procedure($dbc);
    }
    else{
        register_procedure($dbc);
    }
}
else {
    header("location: access_page.php?id=login");
}


function register_procedure($dbc){
    $nome = $_POST["nome"];
    $cognome = $_POST["cognome"];
    $email = $_POST["email"];
    $password = $_POST["password_hashed"];
    $citta = $_POST["citta"];
    $indirizzo = $_POST["indirizzo"];
    $user_type = $_POST["user_type"];
    $id_attivita = NULL;

    if(check_parameters($nome) || check_parameters($cognome) || check_parameters($citta)){
        $_SESSION["access_status"] = "I campi Nome, Cognome e Città non devono contenere numeri";

        if($user_type == "manager"){
            header("location: access_page.php?id=register_manager");
            die();
        }
        else{
            header("location: access_page.php?id=register_user");
            die();
        }
    }


    if($user_type == "manager"){
        $nome_attivita = $_POST["nome_attivita"];
        $id_attivita = $dbc->create_activity($nome_attivita, $citta, $indirizzo);

        if($id_attivita == -1){
            $_SESSION["access_status"] = "Non è stato possibile effettuare la registrazione";
            header("location: access_page.php?id=register_manager");
            die();
        }
    }

    $status = $dbc->register_user($nome, $cognome, $password, $email, $id_attivita, $citta, $indirizzo);

    if(!$status && $user_type == "manager"){
        $_SESSION["access_status"] = "Non è stato possibile effettuare la registrazione";
        header("location: access_page.php?id=register_manager");
    }
    else if(!$status && $user_type == "customer"){
        $_SESSION["access_status"] = "Non è stato possibile effettuare la registrazione";
        header("location: access_page.php?id=register_user");
    }
    else if($status){
        $_SESSION["access_status"] = "La registrazione è avvenuta con successo";
        header("location: access_page.php?id=login");
    }
}

function login_procedure($dbc){
    $email = $_POST["email"];
    $password = $_POST["password_hashed"];

    $data = $dbc->login($email, $password);

    if(!$data){
        $_SESSION["access_status"] = "Non è stato possibile effettuare il login";
        header("location: access_page.php?id=login");
    }
    else {
        register_user_data($data);
        if($_SESSION["user_id"] == "customer"){
            header("location: order_customer.php");
        }
        else {
            setcookie("cart", "", time() - 10, "/");
            header("location: menu_manager.php"); 
        }
    }
}
?>