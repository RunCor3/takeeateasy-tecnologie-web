<?php
     require_once "bootstrap.php";

     if(check_login()){
        $templateParams["nome"] = "item_change.php";
        $templateParams["categorie"] = $dbc->get_all_categories();

        if(isset($_GET["item_id"])){
            if($dbc->get_bar_by_dish($_GET["item_id"]) == $_SESSION["idBar"]){
                $templateParams["titolo"] = "Modifica Elemento";
                $item_id = $_GET["item_id"];
                $templateParams["piatto"] = $dbc->get_dish_by_id($item_id);
                $templateParams["action"] = "modify";
            } else {
                $msg = "Non è possibile modificare il piatto specificato";
                header("location: modify_menu.php?msg=".$msg);
            }
        }
        else{
            $templateParams["titolo"] = "Aggiungi Elemento";
            $templateParams["action"] = "add";
        }

        if(isset($_POST["action"])){
            if(isset($_FILES["imgPiatto"]) && $_FILES["imgPiatto"]["size"] > 0){
                list($result, $img) = uploadImage(UPLOAD_DIR, $_FILES["imgPiatto"]);

                if($result != 1){
                     $msg = "Errore inserimento immagine";
                     header("location: modify_menu.php?msg=".$msg);
                     die();
                }
            }
            else{
                $img = $dbc->get_dish_by_id($_POST["idPiatto"])["img"];
            }

            $nomePiatto = $_POST["nomePiatto"];
            $ingredienti = $_POST["ingredienti"];
            $prezzo = $_POST["prezzo"];
            $qta = $_POST["qta"];
            $idCategoria = $_POST["categoria"];
            $idBar = $_SESSION["idBar"];
            
            if($_POST["action"] == "add"){
                $status = $dbc->insert_new_dish($nomePiatto, $ingredienti, $prezzo, $qta, $img, $idBar, $idCategoria);
            }
            else if($_POST["action"] == "modify"){
                $idPiatto = $_POST["idPiatto"];
                $status = $dbc->update_dish($nomePiatto, $ingredienti, $prezzo, $qta, $img, $idBar, $idCategoria, $idPiatto);
            }

            if(!$status){
                $msg = "La modifica o l'aggiunta del piatto non sono avvenute";
            }
            else{
                $msg ="La modifica o l'aggiunta sono avvenute con successo";
            }
            
            header("location: modify_menu.php?msg=".$msg);
        }  
     }
     else{
        header("location: access_page.php?id=login");
     }
     
 
     require "template/base.php";
?>